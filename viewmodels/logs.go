package viewmodels

import "gitlab.com/BIC_Dev/nitrado-service-v2/services/nitrado"

// GetGlobalLogResponse struct
type GetGlobalLogResponse struct {
	PlayerLogs []nitrado.PlayerLog `json:"player_logs"`
	AdminLogs  []nitrado.AdminLog  `json:"admin_logs"`
	KillLogs   []nitrado.KillLog   `json:"kill_logs"`
}
