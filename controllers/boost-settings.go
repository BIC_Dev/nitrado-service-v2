package controllers

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/BIC_Dev/nitrado-service-v2/services/nitrado"
	"gitlab.com/BIC_Dev/nitrado-service-v2/viewmodels"
)

// GetBoostSettings route
// GET /nitrado-service-v2/boost-settings/:id
func (c *Controller) GetBoostSettings(w http.ResponseWriter, r *http.Request) {
	accountName := r.Header.Get("Account-Name")

	authToken, tokenErr := c.GetNitradoAuthToken(accountName)

	if tokenErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: %s: %s", tokenErr.GetMessage(), tokenErr.Error()), c.Log.LogLow)
		SendJSONResponse(w, viewmodels.NewErrorResponse(tokenErr.Error(), tokenErr.GetMessage()), tokenErr.GetStatus())
		return
	}

	vars := mux.Vars(r)
	gameserverID, convErr := strconv.Atoi(vars["id"])

	if convErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: Could not convert id to int: %s", convErr.Error()), c.Log.LogLow)
		SendJSONResponse(w, viewmodels.NewErrorResponse(convErr.Error(), "Could not convert id to int"), http.StatusBadRequest)
		return
	}

	response, getErr := nitrado.GetBoostSettings(authToken, gameserverID)

	if getErr != nil {
		SendJSONResponse(w, viewmodels.NewErrorResponse(getErr.Error(), getErr.GetMessage()), http.StatusBadRequest)
		return
	}

	SendJSONResponse(w, viewmodels.GetBoostSettingsResponse{
		Message:       response.Status,
		BoostSettings: response.Data.Boosting,
	}, http.StatusOK)
	return
}
