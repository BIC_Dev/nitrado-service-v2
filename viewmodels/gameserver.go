package viewmodels

import "gitlab.com/BIC_Dev/nitrado-service-v2/services/nitrado"

// GetGameserverResponse struct
type GetGameserverResponse struct {
	Message    string
	Gameserver *nitrado.Gameserver
}

// StopGameserverRequestBody struct
type StopGameserverRequestBody struct {
	Message     string `json:"message"`
	StopMessage string `json:"stop_message"`
}

//StopGameserverResponse struct
type StopGameserverResponse struct {
	Data *nitrado.StopGameserverResponse
}

// RestartGameserverRequestBody struct
type RestartGameserverRequestBody struct {
	Message        string `json:"message"`
	RestartMessage string `json:"restart_message"`
}

//RestartGameserverResponse struct
type RestartGameserverResponse struct {
	*nitrado.RestartGameserverResponse
}

//GetBanlistResponse struct
type GetBanlistResponse struct {
	Status  string           `json:"status"`
	Message string           `json:"message"`
	Players []nitrado.Player `json:"players"`
}

//GetWhitelistResponse struct
type GetWhitelistResponse struct {
	Status  string           `json:"status"`
	Message string           `json:"message"`
	Players []nitrado.Player `json:"players"`
}
