package nitrado

// BoostSettingsResponse struct
type BoostSettingsResponse struct {
	Status string `json:"status"`
	Data   struct {
		Boosting BoostSettings `json:"boosting"`
	} `json:"data"`
}

// BoostSettings struct
type BoostSettings struct {
	Enabled        bool   `json:"enabled"`
	Code           string `json:"code"`
	Message        string `json:"message"`
	WelcomeMessage string `json:"welcome_message"`
}
