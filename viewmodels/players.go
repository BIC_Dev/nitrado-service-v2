package viewmodels

import "gitlab.com/BIC_Dev/nitrado-service-v2/services/nitrado"

// GetPlayersResponse struct
type GetPlayersResponse struct {
	Message string           `json:"message"`
	Players []nitrado.Player `json:"players"`
}

// SearchPlayersRequestBody struct
type SearchPlayersRequestBody struct {
	PlayerName string `json:"player_name"`
}

// SearchPlayersResponse struct
type SearchPlayersResponse struct {
	Message string           `json:"message"`
	Players []nitrado.Player `json:"players"`
}

// BanPlayerRequestBody struct
type BanPlayerRequestBody struct {
	PlayerID string `json:"player_id"`
}

// BanPlayerResponse struct
type BanPlayerResponse struct {
	*nitrado.PlayerBanResponse
}

// UnbanPlayerRequestBody struct
type UnbanPlayerRequestBody struct {
	PlayerID string `json:"player_id"`
}

// UnbanPlayerResponse struct
type UnbanPlayerResponse struct {
	*nitrado.PlayerUnbanResponse
}

// WhitelistPlayerRequestBody struct
type WhitelistPlayerRequestBody struct {
	PlayerID string `json:"player_id"`
}

// WhitelistPlayerResponse struct
type WhitelistPlayerResponse struct {
	*nitrado.PlayerWhitelistResponse
}

// UnwhitelistPlayerRequestBody struct
type UnwhitelistPlayerRequestBody struct {
	PlayerID string `json:"player_id"`
}

// UnwhitelistPlayerResponse struct
type UnwhitelistPlayerResponse struct {
	*nitrado.PlayerUnwhitelistResponse
}
