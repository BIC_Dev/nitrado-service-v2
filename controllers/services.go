package controllers

import (
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/BIC_Dev/nitrado-service-v2/services/nitrado"
	"gitlab.com/BIC_Dev/nitrado-service-v2/utils"
	"gitlab.com/BIC_Dev/nitrado-service-v2/viewmodels"
)

// ServicesTTL ttl
const ServicesTTL = "3600" // 1 hour

// GetServices route
// GET /nitrado-service-v2/services
func (c *Controller) GetServices(w http.ResponseWriter, r *http.Request) {
	accountName := r.Header.Get("Account-Name")

	authToken, tokenErr := c.GetNitradoAuthToken(accountName)

	if tokenErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: %s: %s", tokenErr.GetMessage(), tokenErr.Error()), c.Log.LogLow)
		SendJSONResponse(w, viewmodels.NewErrorResponse(tokenErr.Error(), tokenErr.GetMessage()), tokenErr.GetStatus())
		return
	}

	cacheKey := fmt.Sprintf("services:%s", accountName)

	var cacheVal string
	var cacheErr *utils.CacheError

	if r.Header.Get("Cache-Control") != "no-cache" {
		// Get gameserver info from cache
		cacheVal, cacheErr = c.Cache.Get(cacheKey)
	}

	if cacheErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: %s: %s", cacheErr.GetMessage(), cacheErr.Error()), c.Log.LogMedium)
	}

	if cacheVal != "" {
		var services []nitrado.Service
		umErr := json.Unmarshal([]byte(cacheVal), &services)

		// Respond with cached gameserver info
		if umErr == nil {
			SendJSONResponse(w, viewmodels.GetServicesResponse{
				Message:  "Found services",
				Services: services,
			}, http.StatusOK)
			return
		}

		c.Log.Log(fmt.Sprintf("ERROR: Unable to convert cached json services response to struct: %s", umErr.Error()), c.Log.LogMedium)
	}

	services, sErr := nitrado.GetServices(authToken)

	if sErr != nil {
		SendJSONResponse(w, viewmodels.NewErrorResponse(sErr.Error(), sErr.GetMessage()), http.StatusBadRequest)
		return
	}

	jsonData, marshalErr := json.Marshal(services.Data.Services)

	if marshalErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: Unable to convert Services struct to JSON: %s", marshalErr.Error()), c.Log.LogMedium)
	} else {
		// Set TTL on cached data
		setCacheErr := c.Cache.Set(cacheKey, string(jsonData), ServicesTTL)

		if setCacheErr != nil {
			c.Log.Log(fmt.Sprintf("ERROR: %s: %s", setCacheErr.GetMessage(), setCacheErr.Error()), c.Log.LogMedium)
		}
	}

	SendJSONResponse(w, viewmodels.GetServicesResponse{
		Message:  "Found services",
		Services: services.Data.Services,
	}, http.StatusOK)
	return
}
