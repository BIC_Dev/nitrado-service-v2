package controllers

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"

	"gitlab.com/BIC_Dev/nitrado-service-v2/cache"
	"gitlab.com/BIC_Dev/nitrado-service-v2/utils"
)

// Controller struct containing DB access and Configs
type Controller struct {
	Config       *utils.Config
	Log          *utils.Log
	ServiceToken string
	Cache        *cache.Cache
}

// SendJSONResponse sends a response to the client
func SendJSONResponse(w http.ResponseWriter, response interface{}, statusCode int) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	json.NewEncoder(w).Encode(response)
}

// GetNitradoAuthToken from the controller struct
func (c *Controller) GetNitradoAuthToken(accountName string) (string, *utils.CacheError) {
	cacheKey := fmt.Sprintf("nitradotoken:%s", accountName)

	token, cErr := c.Cache.Get(cacheKey)

	if cErr != nil {
		return "", cErr
	}

	if token == "" {
		err := utils.NewCacheError(errors.New("not in cache"))
		err.SetMessage("Could not retrieve a Nitrado Auth Token from cache")
		err.SetStatus(http.StatusBadRequest)
		return "", err
	}

	return token, nil
}
