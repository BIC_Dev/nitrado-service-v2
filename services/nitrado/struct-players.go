package nitrado

// PlayersResponse struct
type PlayersResponse struct {
	Status string `json:"status"`
	Data   struct {
		Players []Player `json:"players"`
	} `json:"data"`
}

// Player struct
type Player struct {
	Name       string   `json:"name"`
	ID         string   `json:"id"`
	IDType     string   `json:"id_type"`
	Online     bool     `json:"online"`
	Actions    []string `json:"actions"`
	LastOnline string   `json:"last_online"`
}

// PlayerBanRequestBody struct
type PlayerBanRequestBody struct {
	Identifier string `json:"identifier"`
}

// PlayerBanResponse struct
type PlayerBanResponse struct {
	Status  string `json:"status"`
	Message string `json:"message"`
}

// PlayerUnbanRequestBody struct
type PlayerUnbanRequestBody struct {
	Identifier string `json:"identifier"`
}

// PlayerUnbanResponse struct
type PlayerUnbanResponse struct {
	Status  string `json:"status"`
	Message string `json:"message"`
}

// PlayerWhitelistRequestBody struct
type PlayerWhitelistRequestBody struct {
	Identifier string `json:"identifier"`
}

// PlayerWhitelistResponse struct
type PlayerWhitelistResponse struct {
	Status  string `json:"status"`
	Message string `json:"message"`
}

// PlayerUnwhitelistRequestBody struct
type PlayerUnwhitelistRequestBody struct {
	Identifier string `json:"identifier"`
}

// PlayerUnwhitelistResponse struct
type PlayerUnwhitelistResponse struct {
	Status  string `json:"status"`
	Message string `json:"message"`
}
