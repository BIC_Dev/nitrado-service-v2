package controllers

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/secretsmanager"
	"gitlab.com/BIC_Dev/nitrado-service-v2/viewmodels"
)

// UpdateNitradoTokens retrieves new Nitrado Tokens from Secrets Manager
func (c *Controller) UpdateNitradoTokens(w http.ResponseWriter, r *http.Request) {
	sess := session.Must(session.NewSession(&aws.Config{
		Region: aws.String(c.Config.AWS.Region),
	}))

	svc := secretsmanager.New(sess)
	input := &secretsmanager.GetSecretValueInput{
		SecretId: aws.String(c.Config.AWS.NitradoTokensSecretArn),
	}

	result, sErr := svc.GetSecretValue(input)

	if sErr != nil {
		SendJSONResponse(w, viewmodels.NewErrorResponse(sErr.Error(), "Failed to update Nitrado tokens"), http.StatusFailedDependency)
		return
	}

	var tokens map[string]string

	jsonErr := json.Unmarshal([]byte(*result.SecretString), &tokens)

	if jsonErr != nil {
		SendJSONResponse(w, viewmodels.NewErrorResponse(jsonErr.Error(), "Failed to unmarshal Nitrado tokens"), http.StatusInternalServerError)
		return
	}

	var updatedNitradoTokens []string
	var failedNitradoTokens []string

	for key, val := range tokens {
		cacheKey := fmt.Sprintf("nitradotoken:%s", key)
		cErr := c.Cache.Set(cacheKey, val, "")

		if cErr != nil {
			failedNitradoTokens = append(failedNitradoTokens, key)
			c.Log.Log(fmt.Sprintf("ERROR: Failed to cache Nitrado token: %s", key), c.Log.LogHigh)
			continue
		}

		updatedNitradoTokens = append(updatedNitradoTokens, key)
	}

	SendJSONResponse(w, viewmodels.UpdateNitradoTokensResponse{
		Message:       "Updated Nitrado tokens in cache",
		UpdatedTokens: updatedNitradoTokens,
		FailedTokens:  failedNitradoTokens,
	}, http.StatusOK)
}
