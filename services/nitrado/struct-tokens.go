package nitrado

// TokenInfoResponse struct
type TokenInfoResponse struct {
	Status string `json:"status"`
	Data   struct {
		Token Token `json:"token"`
	} `json:"data"`
}

// Token struct
type Token struct {
	ID   int `json:"id"`
	User struct {
		ID       int    `json:"id"`
		Username string `json:"username"`
	} `json:"user"`
	ExpiresAt       int64    `json:"expires_at"`
	ValidUntil      string   `json:"valid_until"`
	Scopes          []string `json:"scopes"`
	TwoFactorMethod string   `json:"two_factor_method"`
	Employee        bool     `json:"employee"`
}
