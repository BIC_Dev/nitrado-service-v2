package routes

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/BIC_Dev/nitrado-service-v2/cache"
	"gitlab.com/BIC_Dev/nitrado-service-v2/controllers"
	"gitlab.com/BIC_Dev/nitrado-service-v2/utils"
	"gitlab.com/BIC_Dev/nitrado-service-v2/viewmodels"
)

type middleware struct {
	Log          *utils.Log
	ServiceToken string
	Cache        *cache.Cache
}

// GetRouter creates and returns a router
func GetRouter() *mux.Router {
	return mux.NewRouter().StrictSlash(true)
}

// AddRoutes adds all necessary routes to the router
func AddRoutes(router *mux.Router, c *controllers.Controller) {
	middleware := middleware{
		Log:          c.Log,
		ServiceToken: c.ServiceToken,
		Cache:        c.Cache,
	}

	router.HandleFunc("/nitrado-service-v2/status", c.GetStatus).Methods("GET")

	router.HandleFunc("/nitrado-service-v2/update/tokens", c.UpdateNitradoTokens).Methods("GET")

	router.HandleFunc("/nitrado-service-v2/gameserver/{gameserver_id}", c.GetGameserver).Methods("GET")
	router.HandleFunc("/nitrado-service-v2/gameserver/{gameserver_id}/banlist", c.GetBanlist).Methods("GET")
	router.HandleFunc("/nitrado-service-v2/gameserver/{gameserver_id}/whitelist", c.GetWhitelist).Methods("GET")
	router.HandleFunc("/nitrado-service-v2/gameserver/{gameserver_id}/stop", c.StopGameserver).Methods("POST")
	router.HandleFunc("/nitrado-service-v2/gameserver/{gameserver_id}/restart", c.RestartGameserver).Methods("POST")

	router.HandleFunc("/nitrado-service-v2/players/{gameserver_id}", c.GetPlayers).Methods("GET")
	router.HandleFunc("/nitrado-service-v2/players/{gameserver_id}/search", c.SearchPlayersByName).Methods("POST")
	router.HandleFunc("/nitrado-service-v2/players/{gameserver_id}/ban", c.BanPlayer).Methods("POST")
	router.HandleFunc("/nitrado-service-v2/players/{gameserver_id}/unban", c.UnBanPlayer).Methods("DELETE")
	router.HandleFunc("/nitrado-service-v2/players/{gameserver_id}/whitelist", c.WhitelistPlayer).Methods("POST")
	router.HandleFunc("/nitrado-service-v2/players/{gameserver_id}/whitelist", c.UnwhitelistPlayer).Methods("DELETE")

	router.HandleFunc("/nitrado-service-v2/logs/{gameserver_id}", c.GetGlobalLog).Methods("GET")

	router.HandleFunc("/nitrado-service-v2/services", c.GetServices).Methods("GET")

	router.HandleFunc("/nitrado-service-v2/boost-settings/{id}", c.GetBoostSettings).Methods("GET")

	router.HandleFunc("/nitrado-service-v2/nitrado-tokens", c.CreateNitradoToken).Methods("POST")
	router.HandleFunc("/nitrado-service-v2/nitrado-tokens", c.UpdateNitradoToken).Methods("PUT")

	router.HandleFunc("/nitrado-service-v2/file-descriptor-count", c.GetFileDescriptorCounts).Methods("GET")

	router.Use(middleware.loggingMiddleware)
	router.Use(middleware.authenticationMiddleware)
	router.Use(middleware.accountNameMiddleware)
}

// StartListener starts the HTTP listener
func StartListener(router *mux.Router, port string) {
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", port), router))
}

// Logs the request
func (m *middleware) loggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.RequestURI == "/nitrado-service-v2/status" {
			next.ServeHTTP(w, r)
		} else {
			m.Log.Log(fmt.Sprintf("ROUTE: Request to route %s", r.RequestURI), m.Log.LogInformation)
			next.ServeHTTP(w, r)
		}
	})
}

// Verifies the Service-Token header is set and authorized for access to the API
func (m *middleware) authenticationMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		serviceTokenHeader := r.Header.Get("Service-Token")
		if r.RequestURI == "/nitrado-service-v2/status" {
			next.ServeHTTP(w, r)
		} else if serviceTokenHeader == "" {
			m.Log.Log(fmt.Sprintf("ERROR: Missing Service-Token header for route %s", r.RequestURI), m.Log.LogLow)
			controllers.SendJSONResponse(w, viewmodels.ErrorResponse{
				StatusCode: http.StatusUnauthorized,
				Error:      "Missing Service-Token header",
				Message:    "A Service-Token header must be set for all routes",
			}, http.StatusUnauthorized)
		} else if serviceTokenHeader == m.ServiceToken {
			next.ServeHTTP(w, r)
		} else {
			m.Log.Log(fmt.Sprintf("ERROR: Invalid Service-Token header for route %s", r.RequestURI), m.Log.LogLow)
			controllers.SendJSONResponse(w, viewmodels.ErrorResponse{
				StatusCode: http.StatusUnauthorized,
				Error:      "Invalid Service-Token header",
				Message:    "An invalid Service-Token header was sent with request",
			}, http.StatusUnauthorized)
		}
	})
}

// Verifies the Account-Name header is set
func (m *middleware) accountNameMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		accountNameHeader := r.Header.Get("Account-Name")
		if r.RequestURI == "/nitrado-service-v2/status" || r.RequestURI == "/nitrado-service-v2/update/tokens" || r.RequestURI == "/nitrado-service-v2/nitrado-tokens" || r.RequestURI == "/nitrado-service-v2/file-descriptor-count" {
			next.ServeHTTP(w, r)
		} else if accountNameHeader == "" {
			m.Log.Log(fmt.Sprintf("ERROR: Missing Account-Name header for route %s", r.RequestURI), m.Log.LogLow)
			controllers.SendJSONResponse(w, viewmodels.ErrorResponse{
				StatusCode: http.StatusUnauthorized,
				Error:      "Missing Account-Name header",
				Message:    "An Account-Name header must be set for all routes",
			}, http.StatusUnauthorized)
		} else {
			next.ServeHTTP(w, r)
		}
	})
}
