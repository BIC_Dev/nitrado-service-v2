package controllers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/BIC_Dev/nitrado-service-v2/services/nitrado"
	"gitlab.com/BIC_Dev/nitrado-service-v2/utils"
	"gitlab.com/BIC_Dev/nitrado-service-v2/viewmodels"
)

// UpdatedLogFiles struct
type UpdatedLogFiles struct {
	ShooterGameUpdated     bool
	ShooterGameLastUpdated bool
	LogFiles               LogFiles
}

// LogFiles struct
type LogFiles struct {
	ShooterGame     nitrado.FileDetailsEntry `json:"shooter_game"`
	ShooterGameLast nitrado.FileDetailsEntry `json:"shooter_game_last"`
	ModifiedAt      int64                    `json:"modified_at"`
	LatestTimestamp int64                    `json:"latest_timestamp"`
}

const logMetadataTTL = "604800" // 1 week

// GetGlobalLog route
func (c *Controller) GetGlobalLog(w http.ResponseWriter, r *http.Request) {
	accountName := r.Header.Get("Account-Name")

	authToken, tokenErr := c.GetNitradoAuthToken(accountName)

	if tokenErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: %s: %s", tokenErr.GetMessage(), tokenErr.Error()), c.Log.LogLow)
		SendJSONResponse(w, viewmodels.NewErrorResponse(tokenErr.Error(), tokenErr.GetMessage()), tokenErr.GetStatus())
		return
	}

	vars := mux.Vars(r)
	gameserverID, convErr := strconv.Atoi(vars["gameserver_id"])

	if convErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: Could not convert gameserver_id to int: %s", convErr.Error()), c.Log.LogLow)
		SendJSONResponse(w, viewmodels.NewErrorResponse(convErr.Error(), "Could not convert gameserver_id to int"), http.StatusBadRequest)
		return
	}

	parseChatLog := false
	parseAdminLog := false
	parseKillLog := false

	if r.URL.Query().Get("chatlog") == "true" {
		parseChatLog = true
	}

	if r.URL.Query().Get("adminlog") == "true" {
		parseAdminLog = true
	}

	if r.URL.Query().Get("killlog") == "true" {
		parseKillLog = true
	}

	if !parseChatLog && !parseAdminLog && !parseKillLog {
		c.Log.Log(fmt.Sprintf("ERROR: No log parsing requested for server: %d", gameserverID), c.Log.LogInformation)
		SendJSONResponse(w, viewmodels.NewErrorResponse("no log parsing requested", "No log parsing requested"), http.StatusBadRequest)
		return
	}

	logMetadataCacheKey := fmt.Sprintf("logmetadata:%d", gameserverID)

	var logMetadataString string
	var logMetadataCacheErr *utils.CacheError

	if r.Header.Get("Cache-Control") != "no-cache" {
		// Get gameserver info from cache
		logMetadataString, logMetadataCacheErr = c.Cache.Get(logMetadataCacheKey)
	}

	if logMetadataCacheErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: %s: %s", "Failed to retreive cached metadata", logMetadataCacheErr.Error()), c.Log.LogMedium)
	}

	var logMetadata LogFiles
	var logMetadataErr error

	if logMetadataString != "" {
		logMetadataErr = json.Unmarshal([]byte(logMetadataString), &logMetadata)

		if logMetadataErr != nil {
			c.Log.Log(fmt.Sprintf("ERROR: Failed to unmarshal cached log metdata: %s", logMetadataErr.Error()), c.Log.LogMedium)
			SendJSONResponse(w, viewmodels.NewErrorResponse(logMetadataErr.Error(), "Failed to unmarshal cached log metdata"), http.StatusInternalServerError)
			return
		}
	}

	// Get file path
	gsInfo, gsErr := c.GameserverInfoHandler(authToken, gameserverID, "")

	if gsErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: %s: %s", gsErr.GetMessage(), gsErr.Error()), c.Log.LogMedium)
		SendJSONResponse(w, viewmodels.NewErrorResponse(gsErr.Error(), gsErr.GetMessage()), gsErr.GetStatus())
		return
	}

	logPath, lpErr := GetLogPath(&gsInfo.Data.Gameserver)

	if lpErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: %s: %s", lpErr.GetMessage(), lpErr.Error()), c.Log.LogLow)
		SendJSONResponse(w, viewmodels.NewErrorResponse(lpErr.Error(), lpErr.GetMessage()), lpErr.GetStatus())
		return
	}

	var filePaths []string
	if logPath.CurrentLogFile != "" {
		filePaths = append(filePaths, logPath.CurrentLogFile)
	}
	if logPath.PrevLogFile != "" {
		filePaths = append(filePaths, logPath.PrevLogFile)
	}

	c.Log.Log(fmt.Sprintf("PROCESS: Retreiving log file details for gameserver ID: %d", gameserverID), c.Log.LogInformation)

	statResponse, statErr := nitrado.GetFileDetails(authToken, gameserverID, filePaths)

	if statErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: %s: %s", statErr.GetMessage(), statErr.Error()), c.Log.LogLow)
		SendJSONResponse(w, viewmodels.NewErrorResponse(statErr.Error(), statErr.GetMessage()), statErr.GetStatus())
		return
	}

	logFiles := getUpdatedLogFilePaths(statResponse, logMetadata)

	prevTimestamp := logMetadata.LatestTimestamp

	var logFile *nitrado.DownloadLogFileResponse

	if logFiles.ShooterGameLastUpdated {
		c.Log.Log(fmt.Sprintf("PROCESS: Retreiving previous log file URL for gameserver ID: %d", gameserverID), c.Log.LogInformation)
		aLogFile, aLFErr := getAndDownloadLogFile(authToken, gameserverID, logFiles.LogFiles.ShooterGameLast.Path, prevTimestamp, parseChatLog, parseAdminLog, parseKillLog)

		if aLFErr != nil {
			c.Log.Log(fmt.Sprintf("ERROR: %s: %s", aLFErr.GetMessage(), aLFErr.Error()), c.Log.LogLow)
		} else {
			if logFile == nil {
				logFile = &aLogFile
			} else {
				logFile.AdminLogs = append(logFile.AdminLogs, aLogFile.AdminLogs...)
				logFile.PlayerLogs = append(logFile.PlayerLogs, aLogFile.PlayerLogs...)
				logFile.KillLogs = append(logFile.KillLogs, aLogFile.KillLogs...)
			}
		}
	}

	if logFiles.ShooterGameUpdated {
		c.Log.Log(fmt.Sprintf("PROCESS: Retreiving log file URL for gameserver ID: %d", gameserverID), c.Log.LogInformation)
		aLogFile, aLFErr := getAndDownloadLogFile(authToken, gameserverID, logFiles.LogFiles.ShooterGame.Path, prevTimestamp, parseChatLog, parseAdminLog, parseKillLog)

		if aLFErr != nil {
			c.Log.Log(fmt.Sprintf("ERROR: %s: %s", aLFErr.GetMessage(), aLFErr.Error()), c.Log.LogLow)
		} else {
			if logFile == nil {
				logFile = &aLogFile
			} else {
				logFile.AdminLogs = append(logFile.AdminLogs, aLogFile.AdminLogs...)
				logFile.PlayerLogs = append(logFile.PlayerLogs, aLogFile.PlayerLogs...)
				logFile.KillLogs = append(logFile.KillLogs, aLogFile.KillLogs...)
			}
		}
	}

	var latestTimestamp int64

	if logFile == nil || (len(logFile.AdminLogs) == 0 && len(logFile.PlayerLogs) == 0 && len(logFile.KillLogs) == 0) {
		latestTimestamp = logMetadata.LatestTimestamp
	} else {
		latestTimestamp = getLatestTimestamp(logFile)
	}

	if logFiles.ShooterGameUpdated || logFiles.ShooterGameLastUpdated {
		logFiles.LogFiles.LatestTimestamp = latestTimestamp
		metadataString, metadataErr := json.Marshal(logFiles.LogFiles)

		if metadataErr != nil {
			c.Log.Log(fmt.Sprintf("ERROR: %s: %s", "Failed to marshal log metadata", metadataErr.Error()), c.Log.LogHigh)
		} else {
			cacheSetErr := c.Cache.Set(logMetadataCacheKey, string(metadataString), logMetadataTTL)

			if cacheSetErr != nil {
				c.Log.Log(fmt.Sprintf("ERROR: %s: %s", cacheSetErr.GetMessage(), cacheSetErr.Error()), c.Log.LogMedium)
			}
		}
	}

	if logFile == nil {
		SendJSONResponse(w, viewmodels.GetGlobalLogResponse{}, http.StatusOK)
		return
	}

	// fmt.Printf("Total Kill Entries: %d\n", len(logFile.KillLogs))
	// playerKillPlayer := 0
	// playerKillPlayerDino := 0
	// playerDinoKillPlayer := 0
	// playerDinoKillPlayerDino := 0
	// wildDinoKillPlayer := 0
	// wildDinoKillPlayerDino := 0

	// for _, killLog := range logFile.KillLogs {
	// 	if killLog.PvEKill {
	// 		if killLog.KilledDinoType != "" {
	// 			wildDinoKillPlayerDino += 1
	// 		} else {
	// 			wildDinoKillPlayer += 1
	// 		}
	// 	} else if killLog.KillerDinoType != "" {
	// 		if killLog.KilledDinoType != "" {
	// 			playerDinoKillPlayerDino += 1
	// 		} else {
	// 			playerDinoKillPlayer += 1
	// 		}
	// 	} else if killLog.KillerDinoType == "" {
	// 		if killLog.KilledDinoType != "" {
	// 			playerKillPlayerDino += 1
	// 		} else {
	// 			playerKillPlayer += 1
	// 			fmt.Printf("PVP Kill: %s (%s) killed %s (%s)\n", killLog.KillerName, killLog.KillerTribe, killLog.KilledName, killLog.KilledTribe)
	// 		}
	// 	}
	// }

	// fmt.Printf("Player Killed Player: %d\n", playerKillPlayer)
	// fmt.Printf("Player Killed Player Dino: %d\n", playerKillPlayerDino)
	// fmt.Printf("Player Dino Killed Player: %d\n", playerDinoKillPlayer)
	// fmt.Printf("Player Dino Killed Player Dino: %d\n", playerDinoKillPlayerDino)
	// fmt.Printf("Wild Dino Killed Player: %d\n", wildDinoKillPlayer)
	// fmt.Printf("Wild Dino Killed Player Dino: %d\n", wildDinoKillPlayerDino)

	// Output new chat messages since previous timestamp

	SendJSONResponse(w, viewmodels.GetGlobalLogResponse{
		PlayerLogs: logFile.PlayerLogs,
		AdminLogs:  logFile.AdminLogs,
		KillLogs:   logFile.KillLogs,
	}, http.StatusOK)
	return
}

// getLatestPlayerLogs func
func getLatestTimestamp(logs *nitrado.DownloadLogFileResponse) int64 {
	var latestTimestamp int64

	for _, log := range logs.PlayerLogs {
		if log.Timestamp > latestTimestamp {
			latestTimestamp = log.Timestamp
		}
	}

	for _, log := range logs.AdminLogs {
		if log.Timestamp > latestTimestamp {
			latestTimestamp = log.Timestamp
		}
	}

	for _, log := range logs.KillLogs {
		if log.Timestamp > latestTimestamp {
			latestTimestamp = log.Timestamp
		}
	}

	return latestTimestamp
}

func getUpdatedLogFilePaths(fl *nitrado.GetFileDetailsResponse, logMetadata LogFiles) UpdatedLogFiles {
	var shooterGameLog string = "ShooterGame.log"
	var shooterGameLastLog string = "ShooterGame_Last.log"

	var updatedLogFiles UpdatedLogFiles

	for _, log := range fl.Data.Entries {
		if log.Name == shooterGameLog {
			updatedLogFiles.LogFiles.ShooterGame = log

			if logMetadata.ShooterGame.Path == "" {
				updatedLogFiles.ShooterGameUpdated = true
			} else if logMetadata.ShooterGame.ModifiedAt < log.ModifiedAt {
				updatedLogFiles.ShooterGameUpdated = true
			} else if logMetadata.ShooterGame.Size < log.Size {
				updatedLogFiles.ShooterGameUpdated = true
			} else {
				continue
			}

			if updatedLogFiles.LogFiles.ModifiedAt < log.ModifiedAt {
				updatedLogFiles.LogFiles.ModifiedAt = log.ModifiedAt
			}
		} else if log.Name == shooterGameLastLog {
			updatedLogFiles.LogFiles.ShooterGameLast = log

			if logMetadata.ShooterGameLast.Path == "" {
				updatedLogFiles.ShooterGameLastUpdated = true
			} else if logMetadata.ShooterGameLast.ModifiedAt < log.ModifiedAt {
				updatedLogFiles.ShooterGameLastUpdated = true
			} else if logMetadata.ShooterGameLast.Size < log.Size {
				updatedLogFiles.ShooterGameLastUpdated = true
			} else {
				continue
			}

			if updatedLogFiles.LogFiles.ModifiedAt < log.ModifiedAt {
				updatedLogFiles.LogFiles.ModifiedAt = log.ModifiedAt
			}
		}
	}

	return updatedLogFiles
}

func getAndDownloadLogFile(authToken string, gameserverID int, logPath string, prevTimestamp int64, parseChatLog bool, parseAdminLog bool, parseKillLog bool) (nitrado.DownloadLogFileResponse, *utils.ServiceError) {
	// Get global log download url
	fdResponse, fdErr := nitrado.GetLogFileURL(authToken, gameserverID, logPath)

	if fdErr != nil {
		return nitrado.DownloadLogFileResponse{}, fdErr
	}

	logFile, dlErr := nitrado.DownloadLogFile(fdResponse.Data.Token.URL, prevTimestamp, parseChatLog, parseAdminLog, parseKillLog)

	if dlErr != nil {
		return nitrado.DownloadLogFileResponse{}, dlErr
	}

	return *logFile, nil
}
