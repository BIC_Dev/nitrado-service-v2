package nitrado

// StopGameserverRequestBody struct
type StopGameserverRequestBody struct {
	Message     string `json:"message"`
	StopMessage string `json:"stop_message"`
}

// StopGameserverResponse struct
type StopGameserverResponse struct {
	Status  string `json:"status"`
	Message string `json:"message"`
}

// RestartGameserverRequestBody struct
type RestartGameserverRequestBody struct {
	Message        string `json:"message"`
	RestartMessage string `json:"restart_message"`
}

// RestartGameserverResponse struct
type RestartGameserverResponse struct {
	Status  string `json:"status"`
	Message string `json:"message"`
}

// GameserverResponse struct
type GameserverResponse struct {
	Status string         `json:"status"`
	Data   GameserverData `json:"data"`
}

// GameserverData struct
type GameserverData struct {
	Gameserver Gameserver `json:"gameserver"`
}

// Gameserver struct
type Gameserver struct {
	MustBeStarted    bool   `json:"must_be_started"`
	Status           string `json:"status"`
	LastStatusChange int    `json:"last_status_change"`
	WebsocketToken   string `json:"websocket_token"`
	Hostsystems      struct {
		Linux struct {
			Hostname string `json:"hostname"`
			Status   string `json:"status"`
		} `json:"linux"`
	} `json:"hostsystems"`
	Username      string `json:"username"`
	ManagedRoot   bool   `json:"managed_root"`
	UserID        int64  `json:"user_id"`
	ServiceID     int64  `json:"service_id"`
	LocationID    int    `json:"location_id"`
	MinecraftMode bool   `json:"minecraft_mode"`
	IP            string `json:"ip"`
	Ipv6          string `json:"ipv6"`
	Port          int    `json:"port"`
	QueryPort     int    `json:"query_port"`
	RconPort      int    `json:"rcon_port"`
	Label         string `json:"label"`
	Type          string `json:"type"`
	Memory        string `json:"memory"`
	MemoryMb      int    `json:"memory_mb"`
	Game          string `json:"game"`
	GameHuman     string `json:"game_human"`
	GameSpecific  struct {
		Path          string      `json:"path"`
		UpdateStatus  string      `json:"update_status"`
		LastUpdate    interface{} `json:"last_update"`
		PathAvailable bool        `json:"path_available"`
		Features      struct {
			HasBackups               bool `json:"has_backups"`
			HasWorldBackups          bool `json:"has_world_backups"`
			HasApplicationServer     bool `json:"has_application_server"`
			HasContainerWebsocket    bool `json:"has_container_websocket"`
			HasRcon                  bool `json:"has_rcon"`
			HasFileBrowser           bool `json:"has_file_browser"`
			HasFtp                   bool `json:"has_ftp"`
			HasExpertMode            bool `json:"has_expert_mode"`
			HasPluginSystem          bool `json:"has_plugin_system"`
			HasRestartMessageSupport bool `json:"has_restart_message_support"`
			HasDatabase              bool `json:"has_database"`
		} `json:"features"`
		LogFiles    []string `json:"log_files"`
		ConfigFiles []string `json:"config_files"`
	} `json:"game_specific"`
	Slots       int    `json:"slots"`
	Location    string `json:"location"`
	Credentials struct {
		Ftp struct {
			Hostname string `json:"hostname"`
			Port     int    `json:"port"`
			Username string `json:"username"`
			Password string `json:"password"`
		} `json:"ftp"`
		Mysql struct {
			Hostname string `json:"hostname"`
			Port     int    `json:"port"`
			Username string `json:"username"`
			Password string `json:"password"`
			Database string `json:"database"`
		} `json:"mysql"`
	} `json:"credentials"`
	Settings struct {
		Config struct {
			ServerName                              string `json:"server-name"`
			AdminPassword                           string `json:"admin-password"`
			ServerPassword                          string `json:"server-password"`
			PlayerOnMap                             string `json:"player-on-map"`
			DisablePvp                              string `json:"disable-pvp"`
			Hardcore                                string `json:"hardcore"`
			Crosshair                               string `json:"crosshair"`
			NoHud                                   string `json:"no-hud"`
			VoiceChat                               string `json:"voice-chat"`
			NearChatOnly                            string `json:"near-chat-only"`
			ThreeRdPerson                           string `json:"3rd-person"`
			LeaveMessages                           string `json:"leave-messages"`
			JoinMessages                            string `json:"join-messages"`
			MessageOfTheDay                         string `json:"message-of-the-day"`
			DifficultyOffset                        string `json:"difficulty-offset"`
			MotdDuration                            string `json:"motd-duration"`
			DisableStructureDecayPve                string `json:"disable-structure-decay-pve"`
			AllowFlyerCarryPve                      string `json:"allow-flyer-carry-pve"`
			MaxStructuresInRange                    string `json:"max-structures-in-range"`
			EnablePvpGamma                          string `json:"enable-pvp-gamma"`
			NoTributeDownloads                      string `json:"no-tribute-downloads"`
			DayCycleSpeedScale                      string `json:"DayCycleSpeedScale"`
			NightTimeSpeedScale                     string `json:"NightTimeSpeedScale"`
			DayTimeSpeedScale                       string `json:"DayTimeSpeedScale"`
			DinoDamageMultiplier                    string `json:"DinoDamageMultiplier"`
			PlayerDamageMultiplier                  string `json:"PlayerDamageMultiplier"`
			StructureDamageMultiplier               string `json:"StructureDamageMultiplier"`
			PlayerResistanceMultiplier              string `json:"PlayerResistanceMultiplier"`
			DinoResistanceMultiplier                string `json:"DinoResistanceMultiplier"`
			StructureResistanceMultiplier           string `json:"StructureResistanceMultiplier"`
			XPMultiplier                            string `json:"XPMultiplier"`
			TamingSpeedMultiplier                   string `json:"TamingSpeedMultiplier"`
			HarvestAmountMultiplier                 string `json:"HarvestAmountMultiplier"`
			HarvestHealthMultiplier                 string `json:"HarvestHealthMultiplier"`
			PlayerCharacterWaterDrainMultiplier     string `json:"PlayerCharacterWaterDrainMultiplier"`
			PlayerCharacterFoodDrainMultiplier      string `json:"PlayerCharacterFoodDrainMultiplier"`
			DinoCharacterFoodDrainMultiplier        string `json:"DinoCharacterFoodDrainMultiplier"`
			PlayerCharacterStaminaDrainMultiplier   string `json:"PlayerCharacterStaminaDrainMultiplier"`
			DinoCharacterStaminaDrainMultiplier     string `json:"DinoCharacterStaminaDrainMultiplier"`
			PlayerCharacterHealthRecoveryMultiplier string `json:"PlayerCharacterHealthRecoveryMultiplier"`
			DinoCharacterHealthRecoveryMultiplier   string `json:"DinoCharacterHealthRecoveryMultiplier"`
			DinoCountMultiplier                     string `json:"DinoCountMultiplier"`
			PvEStructureDecayPeriodMultiplier       string `json:"PvEStructureDecayPeriodMultiplier"`
			ResourcesRespawnPeriodMultiplier        string `json:"ResourcesRespawnPeriodMultiplier"`
			ClampResourceHarvestDamage              string `json:"ClampResourceHarvestDamage"`
			Map                                     string `json:"map"`
			RestartCountdownSeconds                 string `json:"restart-countdown-seconds"`
			ActiveMods                              string `json:"active-mods"`
			StartWithBackup                         string `json:"start-with-backup"`
			PlayersJoinNoCheckList                  string `json:"players-join-no-check-list"`
			BanList                                 string `json:"ban-list"`
			PreventDownloadSurvivors                string `json:"prevent-download-survivors"`
			PreventDownloadItems                    string `json:"prevent-download-items"`
			PreventDownloadDinos                    string `json:"prevent-download-dinos"`
			AdminList                               string `json:"admin-list"`
			DefaultMap                              string `json:"default-map"`
			MapModID                                string `json:"map-mod-id"`
			ActiveTotalConversion                   string `json:"active-total-conversion"`
			DisableDeathSpectator                   string `json:"DisableDeathSpectator"`
			OnlyAdminRejoinAsSpectator              string `json:"OnlyAdminRejoinAsSpectator"`
			BattleNumOfTribesToStartGame            string `json:"BattleNumOfTribesToStartGame"`
			TimeToCollapseROD                       string `json:"TimeToCollapseROD"`
			BattleAutoStartGameInterval             string `json:"BattleAutoStartGameInterval"`
			BattleAutoRestartGameInterval           string `json:"BattleAutoRestartGameInterval"`
			BattleSuddenDeathInterval               string `json:"BattleSuddenDeathInterval"`
			StructureDestructionTag                 string `json:"StructureDestructionTag"`
			ForceRespawnDinos                       string `json:"ForceRespawnDinos"`
			BanListURL                              string `json:"BanListURL"`
			AutoSavePeriodMinutes                   string `json:"AutoSavePeriodMinutes"`
			ActivateAdminLogs                       string `json:"activateAdminLogs"`
			GameLogBuffer                           string `json:"gameLogBuffer"`
			MapExpert                               string `json:"map-expert"`
			DisableDinoDecayPvE                     string `json:"DisableDinoDecayPvE"`
			PvEDinoDecayPeriodMultiplier            string `json:"PvEDinoDecayPeriodMultiplier"`
			DisablePvEGamma                         string `json:"DisablePvEGamma"`
			Exlusivejoin                            string `json:"exlusivejoin"`
			PlayersExclusiveJoinList                string `json:"PlayersExclusiveJoinList"`
			ForceAllStructureLocking                string `json:"ForceAllStructureLocking"`
			AutoDestroyOldStructuresMultiplier      string `json:"AutoDestroyOldStructuresMultiplier"`
			BJoinNotifications                      string `json:"bJoinNotifications"`
			BShowStatusNotificationMessages         string `json:"bShowStatusNotificationMessages"`
			PerPlatformMaxStructuresMultiplier      string `json:"PerPlatformMaxStructuresMultiplier"`
			SpectatorPassword                       string `json:"SpectatorPassword"`
			AllowCaveBuildingPvE                    string `json:"AllowCaveBuildingPvE"`
			Nofishloot                              string `json:"nofishloot"`
			DisableDinoRiding                       string `json:"DisableDinoRiding"`
			DisableDinoTaming                       string `json:"DisableDinoTaming"`
			MaxPersonalTamedDinos                   string `json:"MaxPersonalTamedDinos"`
			OnlyDecayUnsnappedCoreStructures        string `json:"OnlyDecayUnsnappedCoreStructures"`
			TributeItemExpirationSeconds            string `json:"TributeItemExpirationSeconds"`
			TributeDinoExpirationSeconds            string `json:"TributeDinoExpirationSeconds"`
			TributeCharacterExpirationSeconds       string `json:"TributeCharacterExpirationSeconds"`
			CurrentAdminPassword                    string `json:"current-admin-password"`
			ActivateAdminTribeLogs                  string `json:"activateAdminTribeLogs"`
			TribeNameChangeCooldown                 string `json:"TribeNameChangeCooldown"`
			AllowHideDamageSourceFromLogs           string `json:"AllowHideDamageSourceFromLogs"`
			RandomSupplyCratePoints                 string `json:"RandomSupplyCratePoints"`
			DisableWeatherFog                       string `json:"DisableWeatherFog"`
			AdminLogging                            string `json:"AdminLogging"`
			BForceCanRideFliers                     string `json:"bForceCanRideFliers"`
			AllowTekSuitPowersInGenesis             string `json:"AllowTekSuitPowersInGenesis"`
			EnableCryoSicknessPVE                   string `json:"EnableCryoSicknessPVE"`
			CryopodNerfDuration                     string `json:"CryopodNerfDuration"`
			CryopodNerfDamageMult                   string `json:"CryopodNerfDamageMult"`
			ItemStackSizeMultiplier                 string `json:"ItemStackSizeMultiplier"`
		} `json:"config"`
		Gameini struct {
			DinoSpawnWeightMultipliers                      string `json:"DinoSpawnWeightMultipliers"`
			OverrideEngramEntries                           string `json:"OverrideEngramEntries"`
			LevelExperienceRampOverrides                    string `json:"LevelExperienceRampOverrides"`
			OverridePlayerLevelEngramPoints                 string `json:"OverridePlayerLevelEngramPoints"`
			TamedDinoClassDamageMultipliers                 string `json:"TamedDinoClassDamageMultipliers"`
			TamedDinoClassResistanceMultipliers             string `json:"TamedDinoClassResistanceMultipliers"`
			ExcludeItemIndices                              string `json:"ExcludeItemIndices"`
			HarvestResourceItemAmountClassMultipliers       string `json:"HarvestResourceItemAmountClassMultipliers"`
			OverrideNamedEngramEntries                      string `json:"OverrideNamedEngramEntries"`
			BOnlyAllowSpecifiedEngrams                      string `json:"bOnlyAllowSpecifiedEngrams"`
			GlobalSpoilingTimeMultiplier                    string `json:"GlobalSpoilingTimeMultiplier"`
			GlobalItemDecompositionTimeMultiplier           string `json:"GlobalItemDecompositionTimeMultiplier"`
			GlobalCorpseDecompositionTimeMultiplier         string `json:"GlobalCorpseDecompositionTimeMultiplier"`
			OverrideMaxExperiencePointsPlayer               string `json:"OverrideMaxExperiencePointsPlayer"`
			OverrideMaxExperiencePointsDino                 string `json:"OverrideMaxExperiencePointsDino"`
			PvPZoneStructureDamageMultiplier                string `json:"PvPZoneStructureDamageMultiplier"`
			BPvEDisableFriendlyFire                         string `json:"bPvEDisableFriendlyFire"`
			ResourceNoReplenishRadiusPlayers                string `json:"ResourceNoReplenishRadiusPlayers"`
			ResourceNoReplenishRadiusStructures             string `json:"ResourceNoReplenishRadiusStructures"`
			BAutoPvETimer                                   string `json:"bAutoPvETimer"`
			BAutoPvEUseSystemTime                           string `json:"bAutoPvEUseSystemTime"`
			AutoPvEStartTimeSeconds                         string `json:"AutoPvEStartTimeSeconds"`
			AutoPvEStopTimeSeconds                          string `json:"AutoPvEStopTimeSeconds"`
			LayEggIntervalMultiplier                        string `json:"LayEggIntervalMultiplier"`
			DinoTurretDamageMultiplier                      string `json:"DinoTurretDamageMultiplier"`
			BDisableLootCrates                              string `json:"bDisableLootCrates"`
			DinoHarvestingDamageMultiplier                  string `json:"DinoHarvestingDamageMultiplier"`
			BDisableFriendlyFire                            string `json:"bDisableFriendlyFire"`
			CustomRecipeEffectivenessMultiplier             string `json:"CustomRecipeEffectivenessMultiplier"`
			CustomRecipeSkillMultiplier                     string `json:"CustomRecipeSkillMultiplier"`
			MatingIntervalMultiplier                        string `json:"MatingIntervalMultiplier"`
			EggHatchSpeedMultiplier                         string `json:"EggHatchSpeedMultiplier"`
			BabyMatureSpeedMultiplier                       string `json:"BabyMatureSpeedMultiplier"`
			BPassiveDefensesDamageRiderlessDinos            string `json:"bPassiveDefensesDamageRiderlessDinos"`
			KillXPMultiplier                                string `json:"KillXPMultiplier"`
			HarvestXPMultiplier                             string `json:"HarvestXPMultiplier"`
			CraftXPMultiplier                               string `json:"CraftXPMultiplier"`
			GenericXPMultiplier                             string `json:"GenericXPMultiplier"`
			SpecialXPMultiplier                             string `json:"SpecialXPMultiplier"`
			PGMapName                                       string `json:"PGMapName"`
			PGTerrainPropertiesString                       string `json:"PGTerrainPropertiesString"`
			ConfigOverrideSupplyCrateItems                  string `json:"ConfigOverrideSupplyCrateItems"`
			BDisableDinoRiding                              string `json:"bDisableDinoRiding"`
			BDisableDinoTaming                              string `json:"bDisableDinoTaming"`
			BUseCorpseLocator                               string `json:"bUseCorpseLocator"`
			BDisableStructurePlacementCollision             string `json:"bDisableStructurePlacementCollision"`
			FastDecayInterval                               string `json:"FastDecayInterval"`
			BUseSingleplayerSettings                        string `json:"bUseSingleplayerSettings"`
			BAllowUnlimitedRespecs                          string `json:"bAllowUnlimitedRespecs"`
			SupplyCrateLootQualityMultiplier                string `json:"SupplyCrateLootQualityMultiplier"`
			FishingLootQualityMultiplier                    string `json:"FishingLootQualityMultiplier"`
			PerLevelStatsMultiplier                         string `json:"PerLevelStatsMultiplier"`
			BabyCuddleIntervalMultiplier                    string `json:"BabyCuddleIntervalMultiplier"`
			BabyCuddleGracePeriodMultiplier                 string `json:"BabyCuddleGracePeriodMultiplier"`
			BabyCuddleLoseImprintQualitySpeedMultiplier     string `json:"BabyCuddleLoseImprintQualitySpeedMultiplier"`
			BabyImprintingStatScaleMultiplier               string `json:"BabyImprintingStatScaleMultiplier"`
			PlayerHarvestingDamageMultiplier                string `json:"PlayerHarvestingDamageMultiplier"`
			CropGrowthSpeedMultiplier                       string `json:"CropGrowthSpeedMultiplier"`
			BabyFoodConsumptionSpeedMultiplier              string `json:"BabyFoodConsumptionSpeedMultiplier"`
			DinoClassDamageMultipliers                      string `json:"DinoClassDamageMultipliers"`
			BPvEAllowTribeWar                               string `json:"bPvEAllowTribeWar"`
			BPvEAllowTribeWarCancel                         string `json:"bPvEAllowTribeWarCancel"`
			CropDecaySpeedMultiplier                        string `json:"CropDecaySpeedMultiplier"`
			HairGrowthSpeedMultiplier                       string `json:"HairGrowthSpeedMultiplier"`
			FuelConsumptionIntervalMultiplier               string `json:"FuelConsumptionIntervalMultiplier"`
			KickIdlePlayersPeriod                           string `json:"KickIdlePlayersPeriod"`
			MaxNumberOfPlayersInTribe                       string `json:"MaxNumberOfPlayersInTribe"`
			UseCorpseLifeSpanMultiplier                     string `json:"UseCorpseLifeSpanMultiplier"`
			GlobalPoweredBatteryDurabilityDecreasePerSecond string `json:"GlobalPoweredBatteryDurabilityDecreasePerSecond"`
			BLimitTurretsInRange                            string `json:"bLimitTurretsInRange"`
			LimitTurretsRange                               string `json:"LimitTurretsRange"`
			LimitTurretsNum                                 string `json:"LimitTurretsNum"`
			BHardLimitTurretsInRange                        string `json:"bHardLimitTurretsInRange"`
			BShowCreativeMode                               string `json:"bShowCreativeMode"`
			PreventOfflinePvPConnectionInvincibleInterval   string `json:"PreventOfflinePvPConnectionInvincibleInterval"`
			TamedDinoCharacterFoodDrainMultiplier           string `json:"TamedDinoCharacterFoodDrainMultiplier"`
			WildDinoCharacterFoodDrainMultiplier            string `json:"WildDinoCharacterFoodDrainMultiplier"`
			WildDinoTorporDrainMultiplier                   string `json:"WildDinoTorporDrainMultiplier"`
			PassiveTameIntervalMultiplier                   string `json:"PassiveTameIntervalMultiplier"`
			TamedDinoTorporDrainMultiplier                  string `json:"TamedDinoTorporDrainMultiplier"`
			BIgnoreStructuresPreventionVolumes              string `json:"bIgnoreStructuresPreventionVolumes"`
			BGenesisUseStructuresPreventionVolumes          string `json:"bGenesisUseStructuresPreventionVolumes"`
			BDisableGenesisMissions                         string `json:"bDisableGenesisMissions"`
			BabyImprintAmountMultiplier                     string `json:"BabyImprintAmountMultiplier"`
		} `json:"gameini"`
		Features struct {
			EngineSettings string `json:"engine-settings"`
		} `json:"features"`
		Append struct {
			Gameini string `json:"gameini"`
		} `json:"append"`
		General struct {
			ExpertMode                          string `json:"expertMode"`
			Battleye                            string `json:"battleye"`
			ServerLanguage                      string `json:"server-language"`
			Vday                                string `json:"vday"`
			ModUpdateList                       string `json:"mod-update-list"`
			ModStatusList                       string `json:"mod-status-list"`
			GameplayLog                         string `json:"gameplay-log"`
			ForceAllowCaveFlyers                string `json:"force-allow-cave-flyers"`
			Vac                                 string `json:"vac"`
			GamesettingsSavedUtcTimestampHidden string `json:"gamesettings_saved_utc_timestamp_hidden"`
			AutomaticUpdateMechanism            string `json:"automatic-update-mechanism"`
			Clusterid                           string `json:"clusterid"`
			Enablecluster                       string `json:"enablecluster"`
			PrimitivePlus                       string `json:"PrimitivePlus"`
			EnableIdlePlayerKick                string `json:"enable-idle-player-kick"`
			NoAntiSpeedHack                     string `json:"no-anti-speed-hack"`
			NoBiomeWalls                        string `json:"no-biome-walls"`
			NotifyAdminCommandsInChat           string `json:"notify-admin-commands-in-chat"`
			Metrics                             string `json:"metrics"`
			CrossPlay                           string `json:"CrossPlay"`
			ActiveEvent                         string `json:"ActiveEvent"`
			Structurememopts                    string `json:"structurememopts"`
			Noundermeshchecking                 string `json:"noundermeshchecking"`
			Noundermeshkilling                  string `json:"noundermeshkilling"`
		} `json:"general"`
		StartParam struct {
			PvPStructureDecay                       string `json:"PvPStructureDecay"`
			PreventOfflinePvP                       string `json:"PreventOfflinePvP"`
			PreventOfflinePvPInterval               string `json:"PreventOfflinePvPInterval"`
			ShowFloatingDamageText                  string `json:"ShowFloatingDamageText"`
			DisableImprintDinoBuff                  string `json:"DisableImprintDinoBuff"`
			AllowAnyoneBabyImprintCuddle            string `json:"AllowAnyoneBabyImprintCuddle"`
			OverideStructurePlatformPrevention      string `json:"OverideStructurePlatformPrevention"`
			EnableExtraStructurePreventionVolumes   string `json:"EnableExtraStructurePreventionVolumes"`
			NonPermanentDiseases                    string `json:"NonPermanentDiseases"`
			PreventDiseases                         string `json:"PreventDiseases"`
			OverrideStructurePlatformPrevention     string `json:"OverrideStructurePlatformPrevention"`
			PreventTribeAlliances                   string `json:"PreventTribeAlliances"`
			AllowRaidDinoFeeding                    string `json:"AllowRaidDinoFeeding"`
			AllowHitMarkers                         string `json:"AllowHitMarkers"`
			FastDecayUnsnappedCoreStructures        string `json:"FastDecayUnsnappedCoreStructures"`
			TribeLogDestroyedEnemyStructures        string `json:"TribeLogDestroyedEnemyStructures"`
			OverrideOfficialDifficulty              string `json:"OverrideOfficialDifficulty"`
			PreventDownloadSurvivors                string `json:"PreventDownloadSurvivors"`
			PreventDownloadItems                    string `json:"PreventDownloadItems"`
			PreventDownloadDinos                    string `json:"PreventDownloadDinos"`
			PreventUploadSurvivors                  string `json:"PreventUploadSurvivors"`
			PreventUploadItems                      string `json:"PreventUploadItems"`
			PreventUploadDinos                      string `json:"PreventUploadDinos"`
			ForceFlyerExplosives                    string `json:"ForceFlyerExplosives"`
			DestroyUnconnectedWaterPipes            string `json:"DestroyUnconnectedWaterPipes"`
			PvPDinoDecay                            string `json:"PvPDinoDecay"`
			PvEAllowStructuresAtSupplyDrops         string `json:"PvEAllowStructuresAtSupplyDrops"`
			AllowCrateSpawnsOnTopOfStructures       string `json:"AllowCrateSpawnsOnTopOfStructures"`
			UseOptimizedHarvestingHealth            string `json:"UseOptimizedHarvestingHealth"`
			ClampItemSpoilingTimes                  string `json:"ClampItemSpoilingTimes"`
			AutoDestroyDecayedDinos                 string `json:"AutoDestroyDecayedDinos"`
			AllowFlyingStaminaRecovery              string `json:"AllowFlyingStaminaRecovery"`
			AllowMultipleAttachedC4                 string `json:"AllowMultipleAttachedC4"`
			BAllowPlatformSaddleMultiFloors         string `json:"bAllowPlatformSaddleMultiFloors"`
			PreventSpawnAnimations                  string `json:"PreventSpawnAnimations"`
			AutoDestroyStructures                   string `json:"AutoDestroyStructures"`
			MinimumDinoReuploadInterval             string `json:"MinimumDinoReuploadInterval"`
			OnlyAutoDestroyCoreStructures           string `json:"OnlyAutoDestroyCoreStructures"`
			OxygenSwimSpeedStatMultiplier           string `json:"OxygenSwimSpeedStatMultiplier"`
			ServerAutoForceRespawnWildDinosInterval string `json:"ServerAutoForceRespawnWildDinosInterval"`
			CrossARKAllowForeignDinoDownloads       string `json:"CrossARKAllowForeignDinoDownloads"`
			ClampItemStats                          string `json:"ClampItemStats"`
			EnableCryopodNerf                       string `json:"EnableCryopodNerf"`
		} `json:"start-param"`
	} `json:"settings"`
	Quota struct {
		BlockUsage     int `json:"block_usage"`
		BlockSoftlimit int `json:"block_softlimit"`
		BlockHardlimit int `json:"block_hardlimit"`
		FileUsage      int `json:"file_usage"`
		FileSoftlimit  int `json:"file_softlimit"`
		FileHardlimit  int `json:"file_hardlimit"`
	} `json:"quota"`
	Query struct {
		ServerName    string `json:"server_name"`
		ConnectIP     string `json:"connect_ip"`
		Map           string `json:"map"`
		Version       string `json:"version"`
		PlayerCurrent int    `json:"player_current"`
		PlayerMax     int    `json:"player_max"`
		Players       []struct {
			ID     int64  `json:"id"`
			Name   string `json:"name"`
			Bot    bool   `json:"bot"`
			Score  int    `json:"score"`
			Frags  int    `json:"frags"`
			Deaths int    `json:"deaths"`
			Time   int    `json:"time"`
			Ping   int    `json:"ping"`
		} `json:"players"`
	} `json:"query"`
}

// GetBanlistResponse struct
type GetBanlistResponse struct {
	Status  string      `json:"status"`
	Message string      `json:"message"`
	Data    BanlistData `json:"data"`
}

// BanlistData struct
type BanlistData struct {
	Players []Player `json:"banlist"`
}

// GetWhitelistResponse struct
type GetWhitelistResponse struct {
	Status  string        `json:"status"`
	Message string        `json:"message"`
	Data    WhitelistData `json:"data"`
}

// WhitelistData struct
type WhitelistData struct {
	Players []Player `json:"whitelist"`
}
