package nitrado

// GetFileListResponse struct
type GetFileListResponse struct {
	Status  string `json:"status"`
	Message string `json:"message"`
	Data    struct {
		Entries []FileListEntry `json:"entries"`
	} `json:"data"`
}

// GetFileDetailsResponse struct
type GetFileDetailsResponse struct {
	Status  string `json:"status"`
	Message string `json:"message"`
	Data    struct {
		Entries map[string]FileDetailsEntry `json:"entries"`
	} `json:"data"`
}

// FileListEntry struct
type FileListEntry struct {
	Group      string `json:"group"`
	AccessedAt int64  `json:"accessed_at"`
	Name       string `json:"name"`
	Type       string `json:"type"`
	Size       int    `json:"size"`
	Owner      string `json:"owner"`
	CreatedAt  int64  `json:"created_at"`
	ModifiedAt int64  `json:"modified_at"`
	Path       string `json:"path"`
}

// FileDetailsEntry struct
type FileDetailsEntry struct {
	ModifiedAt int64  `json:"modified_at"`
	CreatedAt  int64  `json:"created_at"`
	Owner      string `json:"owner"`
	Path       string `json:"path"`
	Name       string `json:"name"`
	Size       int    `json:"size"`
	AccessedAt int64  `json:"accessed_at"`
	Type       string `json:"type"`
	Group      string `json:"group"`
	Chmod      string `json:"chmod"`
}

// GetLogFileURLResponse struct
type GetLogFileURLResponse struct {
	Status  string `json:"status"`
	Message string `json:"message"`
	Data    struct {
		Token struct {
			URL   string `json:"url"`
			Token string `json:"token"`
		} `json:"token"`
	} `json:"data"`
}

// DownloadLogFileResponse struct
type DownloadLogFileResponse struct {
	PlayerLogs []PlayerLog `json:"player_logs"`
	AdminLogs  []AdminLog  `json:"admin_logs"`
	KillLogs   []KillLog   `json:"kill_logs"`
}

// PlayerLog struct
type PlayerLog struct {
	Gamertag  string `json:"gamertag"`
	Name      string `json:"name"`
	Message   string `json:"message"`
	Timestamp int64  `json:"timestamp"`
}

// AdminLog struct
type AdminLog struct {
	Name      string `json:"name"`
	Command   string `json:"command"`
	Timestamp int64  `json:"timestamp"`
}

// KillLog struct
type KillLog struct {
	PvEKill        bool   `json:"pve_kill"`
	KilledName     string `json:"killed_name"`
	KilledLevel    int    `json:"killed_level"`
	KilledDinoType string `json:"killed_dino_type"`
	KilledTribe    string `json:"killed_tribe"`
	KillerName     string `json:"killer_name"`
	KillerLevel    int    `json:"killer_level"`
	KillerDinoType string `json:"killer_dino_type"`
	KillerTribe    string `json:"killer_tribe"`
	Timestamp      int64  `json:"timestamp"`
}
