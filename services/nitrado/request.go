package nitrado

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"gitlab.com/BIC_Dev/nitrado-service-v2/utils"
)

// Timeout default timeout value
const Timeout time.Duration = 30

// LogFileTimeout default timeout value
const LogFileTimeout time.Duration = 60

// Request handler
func Request(output interface{}, method string, url string, params map[string]string, body interface{}, headers map[string]string, timeoutOverride time.Duration) *utils.ServiceError {
	timeout := Timeout
	if timeoutOverride != 0 {
		timeout = timeoutOverride
	}

	httpClient := &http.Client{
		Timeout: time.Second * timeout,
	}

	var requestBody *bytes.Buffer
	var newRequest *http.Request
	var newRequestErr error

	if body != nil {
		jsonBody, jsonErr := json.Marshal(body)

		if jsonErr != nil {
			serviceErr := utils.NewServiceError(jsonErr)
			serviceErr.SetMessage("Could not marshal request struct")
			serviceErr.SetStatus(http.StatusInternalServerError)
			return serviceErr
		}

		requestBody = bytes.NewBuffer(jsonBody)

		newRequest, newRequestErr = http.NewRequest(method, url, requestBody)
	} else {
		newRequest, newRequestErr = http.NewRequest(method, url, nil)
	}

	if newRequestErr != nil {
		serviceErr := utils.NewServiceError(newRequestErr)
		serviceErr.SetMessage("Failed to create request")
		serviceErr.SetStatus(http.StatusInternalServerError)
		return serviceErr
	}

	if params != nil {
		q := newRequest.URL.Query()

		for key, val := range params {
			q.Add(key, val)
		}

		newRequest.URL.RawQuery = q.Encode()
	}

	for k, v := range headers {
		newRequest.Header.Add(k, v)
	}

	response, requestErr := httpClient.Do(newRequest)

	if requestErr != nil {
		serviceErr := utils.NewServiceError(requestErr)
		serviceErr.SetMessage("Request failed")
		serviceErr.SetStatus(http.StatusFailedDependency)
		return serviceErr
	}

	defer response.Body.Close()

	switch response.StatusCode {
	case http.StatusOK:
		jsonErr := json.NewDecoder(response.Body).Decode(&output)

		if jsonErr != nil {
			serviceErr := utils.NewServiceError(jsonErr)
			serviceErr.SetMessage("Invalid JSON data from Nitrado API")
			serviceErr.SetStatus(http.StatusInternalServerError)
			return serviceErr
		}

		return nil
	default:
		bodyBytes, _ := ioutil.ReadAll(response.Body)
		bodyString := string(bodyBytes)

		if bodyString == "" {
			serviceErr := utils.NewServiceError(fmt.Errorf("Unknown error"))
			serviceErr.SetMessage("Bad response from Nitrado API")
			serviceErr.SetStatus(response.StatusCode)
			return serviceErr
		}

		var errorResponse map[string]interface{}
		jsonErr := json.Unmarshal([]byte(bodyString), &errorResponse)
		if jsonErr != nil {
			serviceErr := utils.NewServiceError(fmt.Errorf("Unknown error"))
			serviceErr.SetMessage("Bad response from Nitrado API")
			serviceErr.SetStatus(response.StatusCode)
			return serviceErr
		}

		if _, ok := errorResponse["message"]; !ok {
			serviceErr := utils.NewServiceError(fmt.Errorf("Unknown error"))
			serviceErr.SetMessage("Bad response from Nitrado API")
			serviceErr.SetStatus(response.StatusCode)
			return serviceErr
		}

		serviceErr := utils.NewServiceError(errors.New(fmt.Sprint(errorResponse["message"])))
		serviceErr.SetMessage("Bad response from Nitrado API")
		serviceErr.SetStatus(response.StatusCode)
		return serviceErr
	}
}

// RequestLogFile func
func RequestLogFile(logPath string) (string, *utils.ServiceError) {
	httpClient := &http.Client{
		Timeout: time.Second * LogFileTimeout,
	}

	newRequest, newRequestErr := http.NewRequest("GET", logPath, nil)

	if newRequestErr != nil {
		serviceErr := utils.NewServiceError(newRequestErr)
		serviceErr.SetMessage("Failed to create request")
		serviceErr.SetStatus(http.StatusInternalServerError)
		return "", serviceErr
	}

	response, requestErr := httpClient.Do(newRequest)

	if requestErr != nil {
		serviceErr := utils.NewServiceError(requestErr)
		serviceErr.SetMessage("Request failed")
		serviceErr.SetStatus(http.StatusFailedDependency)
		return "", serviceErr
	}

	defer response.Body.Close()

	bodyBytes, bodyErr := ioutil.ReadAll(response.Body)
	if bodyErr != nil {
		serviceErr := utils.NewServiceError(bodyErr)
		serviceErr.SetMessage("Failed to get response body")
		serviceErr.SetStatus(http.StatusFailedDependency)
		return "", serviceErr
	}

	bodyString := string(bodyBytes)

	return bodyString, nil
}
