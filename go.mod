module gitlab.com/BIC_Dev/nitrado-service-v2

go 1.16

require (
	github.com/aws/aws-sdk-go v1.33.16
	github.com/google/uuid v1.2.0
	github.com/gorilla/mux v1.7.4
	github.com/mediocregopher/radix/v3 v3.5.2
	gopkg.in/yaml.v2 v2.3.0
)
