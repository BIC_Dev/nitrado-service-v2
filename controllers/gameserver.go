package controllers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"github.com/gorilla/mux"
	"gitlab.com/BIC_Dev/nitrado-service-v2/services/nitrado"
	"gitlab.com/BIC_Dev/nitrado-service-v2/utils"
	"gitlab.com/BIC_Dev/nitrado-service-v2/viewmodels"
)

const getGameserverTTL = "43200" // 12 hours
const getBanlistTTL = "300"      // 5 minutes
const getWhitelistTTL = "300"    // 5 minutes

// LogPaths struct
type LogPaths struct {
	LogPath        string
	CurrentLogFile string
	PrevLogFile    string
}

// GetGameserver route
// GET /nitrado-service-v2/gameserver/{gameserver_id}
func (c *Controller) GetGameserver(w http.ResponseWriter, r *http.Request) {
	accountName := r.Header.Get("Account-Name")

	authToken, tokenErr := c.GetNitradoAuthToken(accountName)

	if tokenErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: %s: %s", tokenErr.GetMessage(), tokenErr.Error()), c.Log.LogLow)
		SendJSONResponse(w, viewmodels.NewErrorResponse(tokenErr.Error(), tokenErr.GetMessage()), tokenErr.GetStatus())
		return
	}

	vars := mux.Vars(r)
	gameserverID, convErr := strconv.Atoi(vars["gameserver_id"])

	if convErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: Could not convert gameserver_id to int: %s", convErr.Error()), c.Log.LogLow)
		SendJSONResponse(w, viewmodels.NewErrorResponse(convErr.Error(), "Could not convert gameserver_id to int"), http.StatusBadRequest)
		return
	}

	response, getErr := c.GameserverInfoHandler(authToken, gameserverID, r.Header.Get("Cache-Control"))

	if getErr != nil {
		SendJSONResponse(w, viewmodels.NewErrorResponse(getErr.Error(), getErr.GetMessage()), http.StatusBadRequest)
		return
	}

	SendJSONResponse(w, response, http.StatusOK)
	return
}

// StopGameserver route
func (c *Controller) StopGameserver(w http.ResponseWriter, r *http.Request) {
	accountName := r.Header.Get("Account-Name")

	authToken, tokenErr := c.GetNitradoAuthToken(accountName)

	if tokenErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: %s: %s", tokenErr.GetMessage(), tokenErr.Error()), c.Log.LogLow)
		SendJSONResponse(w, viewmodels.NewErrorResponse(tokenErr.Error(), tokenErr.GetMessage()), tokenErr.GetStatus())
		return
	}

	vars := mux.Vars(r)
	gameserverID, convErr := strconv.Atoi(vars["gameserver_id"])

	if convErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: Could not convert gameserver_id to int: %s", convErr.Error()), c.Log.LogLow)
		SendJSONResponse(w, viewmodels.NewErrorResponse(convErr.Error(), "Could not convert gameserver_id to int"), http.StatusBadRequest)
		return
	}

	var body viewmodels.StopGameserverRequestBody
	dcErr := json.NewDecoder(r.Body).Decode(&body)

	if dcErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: Could not decode stop gameserver body: %s", dcErr.Error()), c.Log.LogLow)
		SendJSONResponse(w, viewmodels.NewErrorResponse(dcErr.Error(), "Could not decode stop gameserver body"), http.StatusBadRequest)
		return
	}

	response, gsErr := nitrado.StopGameserver(authToken, gameserverID, body.Message, body.StopMessage)

	if gsErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: Could not make successful request to stop gameserver for ID (%d): %s", gameserverID, gsErr.Error()), c.Log.LogLow)
		SendJSONResponse(w, viewmodels.NewErrorResponse(gsErr.Error(), fmt.Sprintf("ERROR: Could not make successful request to stop gameserver for ID: %d", gameserverID)), http.StatusFailedDependency)
		return
	}

	if response.Status != "success" {
		c.Log.Log(fmt.Sprintf("ERROR: Failed to stop gameserver with ID (%d): %s", gameserverID, response.Message), c.Log.LogLow)
		SendJSONResponse(w, viewmodels.NewErrorResponse(response.Message, fmt.Sprintf("ERROR: Failed to stop gameserver with ID: %d", gameserverID)), http.StatusFailedDependency)
		return
	}

	SendJSONResponse(w, viewmodels.StopGameserverResponse{Data: response}, http.StatusOK)
	return
}

// RestartGameserver route
func (c *Controller) RestartGameserver(w http.ResponseWriter, r *http.Request) {
	accountName := r.Header.Get("Account-Name")

	authToken, tokenErr := c.GetNitradoAuthToken(accountName)

	if tokenErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: %s: %s", tokenErr.GetMessage(), tokenErr.Error()), c.Log.LogLow)
		SendJSONResponse(w, viewmodels.NewErrorResponse(tokenErr.Error(), tokenErr.GetMessage()), tokenErr.GetStatus())
		return
	}

	vars := mux.Vars(r)
	gameserverID, convErr := strconv.Atoi(vars["gameserver_id"])

	if convErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: Could not convert gameserver_id to int: %s", convErr.Error()), c.Log.LogLow)
		SendJSONResponse(w, viewmodels.NewErrorResponse(convErr.Error(), fmt.Sprintf("ERROR: Could not make successful request to restart gameserver for ID: %d", gameserverID)), http.StatusBadRequest)
		return
	}

	var body viewmodels.RestartGameserverRequestBody
	dcErr := json.NewDecoder(r.Body).Decode(&body)

	if dcErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: Could not decode restart gameserver body: %s", dcErr.Error()), c.Log.LogLow)
		SendJSONResponse(w, viewmodels.NewErrorResponse(dcErr.Error(), "Could not decode restart gameserver body"), http.StatusBadRequest)
		return
	}

	response, gsErr := nitrado.RestartGameserver(authToken, gameserverID, body.Message, body.RestartMessage)

	if gsErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: Could not get gameserver data for gameserver ID (%d): %s", gameserverID, gsErr.Error()), c.Log.LogLow)
		SendJSONResponse(w, viewmodels.NewErrorResponse(gsErr.Error(), "Could not convert gameserver_id to int"), http.StatusBadRequest)
		return
	}

	if response.Status != "success" {
		c.Log.Log(fmt.Sprintf("ERROR: Failed to restart gameserver with ID (%d): %s", gameserverID, response.Message), c.Log.LogLow)
		SendJSONResponse(w, viewmodels.NewErrorResponse(response.Message, fmt.Sprintf("ERROR: Failed to restart gameserver with ID: %d", gameserverID)), http.StatusFailedDependency)
		return
	}

	SendJSONResponse(w, viewmodels.RestartGameserverResponse{response}, http.StatusOK)
	return
}

// GetBanlist route
// GET /nitrado-service-v2/gameserver/{gameserver_id}/banlist
func (c *Controller) GetBanlist(w http.ResponseWriter, r *http.Request) {
	accountName := r.Header.Get("Account-Name")

	authToken, tokenErr := c.GetNitradoAuthToken(accountName)

	if tokenErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: %s: %s", tokenErr.GetMessage(), tokenErr.Error()), c.Log.LogLow)
		SendJSONResponse(w, viewmodels.NewErrorResponse(tokenErr.Error(), tokenErr.GetMessage()), tokenErr.GetStatus())
		return
	}

	vars := mux.Vars(r)
	gameserverID, convErr := strconv.Atoi(vars["gameserver_id"])

	if convErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: Could not convert gameserver_id to int: %s", convErr.Error()), c.Log.LogLow)
		SendJSONResponse(w, viewmodels.NewErrorResponse(convErr.Error(), "Could not convert gameserver_id to int"), http.StatusBadRequest)
		return
	}

	cacheKey := fmt.Sprintf("banlist:%d", gameserverID)

	var cacheVal string
	var cacheErr *utils.CacheError

	if r.Header.Get("Cache-Control") != "no-cache" {
		// Get banlist info from cache
		cacheVal, cacheErr = c.Cache.Get(cacheKey)
	}

	if cacheErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: %s: %s", cacheErr.GetMessage(), cacheErr.Error()), c.Log.LogMedium)
	}

	if cacheVal != "" {
		var banlist nitrado.GetBanlistResponse
		umErr := json.Unmarshal([]byte(cacheVal), &banlist)

		// Respond with cached gameserver info
		if umErr == nil {
			var bl viewmodels.GetBanlistResponse = viewmodels.GetBanlistResponse{
				Status:  banlist.Status,
				Message: banlist.Message,
			}

			for _, player := range banlist.Data.Players {
				bl.Players = append(bl.Players, player)
			}

			SendJSONResponse(w, bl, http.StatusOK)
			return
		}

		c.Log.Log(fmt.Sprintf("ERROR: Unable to convert cached json banlist response to struct: %s", umErr.Error()), c.Log.LogMedium)
	}

	response, getErr := nitrado.GetBanlist(authToken, gameserverID)

	if getErr != nil {
		SendJSONResponse(w, viewmodels.NewErrorResponse(getErr.Error(), getErr.GetMessage()), http.StatusBadRequest)
		return
	}

	jsonData, marshalErr := json.Marshal(response)

	if marshalErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: Unable to convert Banlist struct to JSON: %s", marshalErr.Error()), c.Log.LogMedium)
	} else {
		// Set TTL on cached data
		setCacheErr := c.Cache.Set(cacheKey, string(jsonData), getBanlistTTL)

		if setCacheErr != nil {
			c.Log.Log(fmt.Sprintf("ERROR: %s: %s", setCacheErr.GetMessage(), setCacheErr.Error()), c.Log.LogMedium)
		}
	}

	var bl viewmodels.GetBanlistResponse = viewmodels.GetBanlistResponse{
		Status:  response.Status,
		Message: response.Message,
	}

	for _, player := range response.Data.Players {
		bl.Players = append(bl.Players, player)
	}

	SendJSONResponse(w, bl, http.StatusOK)
	return
}

// GetWhitelist route
// GET /nitrado-service-v2/gameserver/{gameserver_id}/whitelist
func (c *Controller) GetWhitelist(w http.ResponseWriter, r *http.Request) {
	accountName := r.Header.Get("Account-Name")

	authToken, tokenErr := c.GetNitradoAuthToken(accountName)

	if tokenErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: %s: %s", tokenErr.GetMessage(), tokenErr.Error()), c.Log.LogLow)
		SendJSONResponse(w, viewmodels.NewErrorResponse(tokenErr.Error(), tokenErr.GetMessage()), tokenErr.GetStatus())
		return
	}

	vars := mux.Vars(r)
	gameserverID, convErr := strconv.Atoi(vars["gameserver_id"])

	if convErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: Could not convert gameserver_id to int: %s", convErr.Error()), c.Log.LogLow)
		SendJSONResponse(w, viewmodels.NewErrorResponse(convErr.Error(), "Could not convert gameserver_id to int"), http.StatusBadRequest)
		return
	}

	cacheKey := fmt.Sprintf("whitelist:%d", gameserverID)

	var cacheVal string
	var cacheErr *utils.CacheError

	if r.Header.Get("Cache-Control") != "no-cache" {
		// Get banlist info from cache
		cacheVal, cacheErr = c.Cache.Get(cacheKey)
	}

	if cacheErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: %s: %s", cacheErr.GetMessage(), cacheErr.Error()), c.Log.LogMedium)
	}

	if cacheVal != "" {
		var whitelist nitrado.GetWhitelistResponse
		umErr := json.Unmarshal([]byte(cacheVal), &whitelist)

		// Respond with cached gameserver info
		if umErr == nil {
			var wl viewmodels.GetWhitelistResponse = viewmodels.GetWhitelistResponse{
				Status:  whitelist.Status,
				Message: whitelist.Message,
			}

			for _, player := range whitelist.Data.Players {
				wl.Players = append(wl.Players, player)
			}

			SendJSONResponse(w, wl, http.StatusOK)
			return
		}

		c.Log.Log(fmt.Sprintf("ERROR: Unable to convert cached json whitelist response to struct: %s", umErr.Error()), c.Log.LogMedium)
	}

	response, getErr := nitrado.GetWhitelist(authToken, gameserverID)

	if getErr != nil {
		SendJSONResponse(w, viewmodels.NewErrorResponse(getErr.Error(), getErr.GetMessage()), http.StatusBadRequest)
		return
	}

	jsonData, marshalErr := json.Marshal(response)

	if marshalErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: Unable to convert whitelist struct to JSON: %s", marshalErr.Error()), c.Log.LogMedium)
	} else {
		// Set TTL on cached data
		setCacheErr := c.Cache.Set(cacheKey, string(jsonData), getWhitelistTTL)

		if setCacheErr != nil {
			c.Log.Log(fmt.Sprintf("ERROR: %s: %s", setCacheErr.GetMessage(), setCacheErr.Error()), c.Log.LogMedium)
		}
	}

	var bl viewmodels.GetBanlistResponse = viewmodels.GetBanlistResponse{
		Status:  response.Status,
		Message: response.Message,
	}

	for _, player := range response.Data.Players {
		bl.Players = append(bl.Players, player)
	}

	SendJSONResponse(w, bl, http.StatusOK)
	return
}

// GameserverInfoHandler gets the gameserver info from cache or nitrado and updates cache
func (c *Controller) GameserverInfoHandler(authToken string, gameserverID int, cacheControl string) (*nitrado.GameserverResponse, *utils.ServiceError) {
	cacheKey := fmt.Sprintf("gameserver:%d", gameserverID)

	var cacheVal string
	var cacheErr *utils.CacheError

	if cacheControl != "no-cache" {
		// Get gameserver info from cache
		cacheVal, cacheErr = c.Cache.Get(cacheKey)
	}

	if cacheErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: %s: %s", cacheErr.GetMessage(), cacheErr.Error()), c.Log.LogMedium)
	}

	if cacheVal != "" {
		var gameserver nitrado.GameserverResponse
		umErr := json.Unmarshal([]byte(cacheVal), &gameserver)

		// Respond with cached gameserver info
		if umErr == nil {
			return &gameserver, nil
		}

		c.Log.Log(fmt.Sprintf("ERROR: Unable to convert cached json gameserver response to struct: %s", umErr.Error()), c.Log.LogMedium)
	}

	response, gsErr := nitrado.GetGameserver(authToken, gameserverID)

	if gsErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: Could not get gameserver data for gameserver ID (%d): %s", gameserverID, gsErr.Error()), c.Log.LogLow)
		return nil, gsErr
	}

	jsonData, marshalErr := json.Marshal(response)

	if marshalErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: Unable to convert GameserverResponse struct to JSON: %s", marshalErr.Error()), c.Log.LogMedium)
	} else {
		// Set TTL on cached data
		setCacheErr := c.Cache.Set(cacheKey, string(jsonData), getGameserverTTL)

		if setCacheErr != nil {
			c.Log.Log(fmt.Sprintf("ERROR: %s: %s", setCacheErr.GetMessage(), setCacheErr.Error()), c.Log.LogMedium)
		}
	}

	return response, nil
}

// GetLogPath func
func GetLogPath(gs *nitrado.Gameserver) (LogPaths, *utils.ServiceError) {
	basePath := gs.GameSpecific.Path

	var logPath LogPaths

	logPath.LogPath = fmt.Sprintf("%s%s", basePath, "ShooterGame/Saved/Logs/")

	for _, v := range gs.GameSpecific.LogFiles {
		if strings.Contains(v, "ShooterGame.log") {
			trimmedPath := strings.TrimPrefix(v, "arkxb")
			trimmedPath = strings.TrimPrefix(trimmedPath, "arkps")
			logPath.CurrentLogFile = fmt.Sprintf("%s%s", basePath, trimmedPath)
		} else if strings.Contains(v, "ShooterGame_Last.log") {
			trimmedPath := strings.TrimPrefix(v, "arkxb")
			trimmedPath = strings.TrimPrefix(trimmedPath, "arkps")
			logPath.PrevLogFile = fmt.Sprintf("%s%s", basePath, trimmedPath)
		}
	}

	if logPath.CurrentLogFile == "" && logPath.PrevLogFile == "" {
		se := utils.NewServiceError(fmt.Errorf("No log files in gameserver"))
		se.SetMessage(fmt.Sprintf("No log files for gameserver ID: %d", gs.ServiceID))
		se.SetStatus(http.StatusFailedDependency)

		return LogPaths{}, se
	}

	return logPath, nil
}
