package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"strconv"

	"gitlab.com/BIC_Dev/nitrado-service-v2/cache"
	"gitlab.com/BIC_Dev/nitrado-service-v2/controllers"
	"gitlab.com/BIC_Dev/nitrado-service-v2/routes"
	"gitlab.com/BIC_Dev/nitrado-service-v2/utils"
)

// Service information for the service
type Service struct {
	Config *utils.Config
	Log    *utils.Log
}

func main() {
	env := os.Getenv("ENVIRONMENT")
	logLevel, err := strconv.Atoi(os.Getenv("LOG_LEVEL"))
	serviceToken, serviceTokenExists := os.LookupEnv("SERVICE_TOKEN")
	accountsJSON, accountsJSONExists := os.LookupEnv("NITRADO_TOKENS")
	listenerPort, lpExists := os.LookupEnv("LISTENER_PORT")

	if err != nil {
		log.Fatal(err.Error())
	}

	service := Service{
		Config: utils.GetConfig(env),
		Log:    utils.InitLog(logLevel),
	}

	if !serviceTokenExists {
		service.Log.Log("ERROR: Missing service token", service.Log.LogFatal)
	}

	if !accountsJSONExists {
		service.Log.Log("ERROR: Missing Nitrado account credentials", service.Log.LogFatal)
	}

	if !lpExists {
		service.Log.Log("ERROR: Listener Port is missing", service.Log.LogFatal)
	}

	var nitradoAccounts map[string]string

	unmarshalErr := json.Unmarshal([]byte(accountsJSON), &nitradoAccounts)

	if unmarshalErr != nil {
		service.Log.Log(fmt.Sprintf("ERROR: Could not unmarshal nitrado accounts with error: %s", unmarshalErr.Error()), service.Log.LogFatal)
	}

	pool, cacheErr := cache.GetClient(service.Config.Redis.Host, service.Config.Redis.Port, service.Config.Redis.Pool)

	if cacheErr != nil {
		service.Log.Log(fmt.Sprintf("ERROR: %s: %s", cacheErr.GetMessage(), cacheErr.Error()), service.Log.LogFatal)
	}

	cache := cache.Cache{
		Client: pool,
		Prefix: service.Config.Redis.Prefix,
	}

	controller := controllers.Controller{
		Config:       service.Config,
		Log:          service.Log,
		ServiceToken: serviceToken,
		Cache:        &cache,
	}

	cErr := service.addNitradoTokensToCache(&cache, nitradoAccounts)

	if cErr != nil {
		service.Log.Log(fmt.Sprintf("ERROR: %s: %s", cErr.GetMessage(), cErr.Error()), service.Log.LogFatal)
	}

	router := routes.GetRouter()
	routes.AddRoutes(router, &controller)

	service.Log.Log("SUCCESS: Set up router", service.Log.LogInformation)

	service.Log.Log("PROCESS: Starting MUX listener", service.Log.LogInformation)
	routes.StartListener(router, listenerPort)
}

func (s *Service) addNitradoTokensToCache(c *cache.Cache, tokens map[string]string) *utils.CacheError {
	for k, v := range tokens {
		cacheKey := fmt.Sprintf("nitradotoken:%s", k)

		cErr := c.Set(cacheKey, v, "")

		if cErr != nil {
			s.Log.Log(fmt.Sprintf("ERROR: Failed to add Nitrado token to cache: %s", k), s.Log.LogInformation)
			return cErr
		}

		s.Log.Log(fmt.Sprintf("SUCCESS: Added Nitrado token to cache: %s", k), s.Log.LogInformation)
	}

	return nil
}
