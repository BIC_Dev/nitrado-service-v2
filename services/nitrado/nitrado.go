package nitrado

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
	"time"

	"gitlab.com/BIC_Dev/nitrado-service-v2/utils"
)

// BaseURL const
const BaseURL = "https://api.nitrado.net"

// GetServices func
func GetServices(token string) (*ServicesResponse, *utils.ServiceError) {
	url := fmt.Sprintf("%s/services", BaseURL)

	headers := map[string]string{
		"Authorization": "Bearer " + token,
	}

	var response ServicesResponse

	re := Request(&response, "GET", url, nil, nil, headers, 0)

	if re != nil {
		return nil, re
	}

	return &response, nil
}

// GetGameserver func
func GetGameserver(token string, gameserverID int) (*GameserverResponse, *utils.ServiceError) {
	url := fmt.Sprintf("%s/services/%d/gameservers", BaseURL, gameserverID)

	headers := map[string]string{
		"Authorization": "Bearer " + token,
	}

	var response GameserverResponse

	re := Request(&response, "GET", url, nil, nil, headers, 0)

	if re != nil {
		return nil, re
	}

	return &response, nil
}

// StopGameserver func
func StopGameserver(token string, gameserverID int, message string, stopMessage string) (*StopGameserverResponse, *utils.ServiceError) {
	url := fmt.Sprintf("%s/services/%d/gameservers/stop", BaseURL, gameserverID)

	headers := map[string]string{
		"Authorization": "Bearer " + token,
		"Content-Type":  "application/json",
	}

	body := StopGameserverRequestBody{
		Message:     message,
		StopMessage: stopMessage,
	}

	var response StopGameserverResponse

	re := Request(&response, "POST", url, nil, body, headers, 0)

	if re != nil {
		return nil, re
	}

	return &response, nil
}

// RestartGameserver func
func RestartGameserver(token string, gameserverID int, message string, restartMessage string) (*RestartGameserverResponse, *utils.ServiceError) {
	url := fmt.Sprintf("%s/services/%d/gameservers/restart", BaseURL, gameserverID)

	headers := map[string]string{
		"Authorization": "Bearer " + token,
		"Content-Type":  "application/json",
	}

	body := RestartGameserverRequestBody{
		Message:        message,
		RestartMessage: restartMessage,
	}

	var response RestartGameserverResponse

	re := Request(&response, "POST", url, nil, body, headers, 0)

	if re != nil {
		return nil, re
	}

	return &response, nil
}

// GetPlayers func
func GetPlayers(token string, gameserverID int) (*PlayersResponse, *utils.ServiceError) {
	url := fmt.Sprintf("%s/services/%d/gameservers/games/players", BaseURL, gameserverID)

	headers := map[string]string{
		"Authorization": "Bearer " + token,
	}

	var response PlayersResponse

	re := Request(&response, "GET", url, nil, nil, headers, 20)

	if re != nil {
		return nil, re
	}

	return &response, nil
}

// PlayerBan func
func PlayerBan(token string, gameserverID int, playerID string) (*PlayerBanResponse, *utils.ServiceError) {
	url := fmt.Sprintf("%s/services/%d/gameservers/games/banlist", BaseURL, gameserverID)

	headers := map[string]string{
		"Authorization": "Bearer " + token,
		"Content-Type":  "application/json",
	}

	body := PlayerBanRequestBody{
		Identifier: playerID,
	}

	var response PlayerBanResponse

	re := Request(&response, "POST", url, nil, body, headers, 0)

	if re != nil {
		return nil, re
	}

	return &response, nil
}

// PlayerUnban func
func PlayerUnban(token string, gameserverID int, playerID string) (*PlayerUnbanResponse, *utils.ServiceError) {
	url := fmt.Sprintf("%s/services/%d/gameservers/games/banlist", BaseURL, gameserverID)

	headers := map[string]string{
		"Authorization": "Bearer " + token,
		"Content-Type":  "application/json",
	}

	body := PlayerUnbanRequestBody{
		Identifier: playerID,
	}

	var response PlayerUnbanResponse

	re := Request(&response, "DELETE", url, nil, body, headers, 0)

	if re != nil {
		return nil, re
	}

	return &response, nil
}

// GetBanlist func
func GetBanlist(token string, gameserverID int) (*GetBanlistResponse, *utils.ServiceError) {
	url := fmt.Sprintf("%s/services/%d/gameservers/games/banlist", BaseURL, gameserverID)

	headers := map[string]string{
		"Authorization": "Bearer " + token,
	}

	var response GetBanlistResponse

	re := Request(&response, "GET", url, nil, nil, headers, 0)

	if re != nil {
		return nil, re
	}

	return &response, nil
}

// GetWhitelist func
func GetWhitelist(token string, gameserverID int) (*GetWhitelistResponse, *utils.ServiceError) {
	url := fmt.Sprintf("%s/services/%d/gameservers/games/whitelist", BaseURL, gameserverID)

	headers := map[string]string{
		"Authorization": "Bearer " + token,
	}

	var response GetWhitelistResponse

	re := Request(&response, "GET", url, nil, nil, headers, 0)

	if re != nil {
		return nil, re
	}

	return &response, nil
}

// GetFileList func
func GetFileList(token string, gameserverID int, folderPath string) (*GetFileListResponse, *utils.ServiceError) {
	url := fmt.Sprintf("%s/services/%d/gameservers/file_server/list", BaseURL, gameserverID)

	headers := map[string]string{
		"Authorization": "Bearer " + token,
	}

	params := map[string]string{
		"dir": folderPath,
	}

	var response GetFileListResponse

	re := Request(&response, "GET", url, params, nil, headers, 0)

	if re != nil {
		return nil, re
	}

	return &response, nil
}

// GetFileDetails func
func GetFileDetails(token string, gameserverID int, filePaths []string) (*GetFileDetailsResponse, *utils.ServiceError) {
	url := fmt.Sprintf("%s/services/%d/gameservers/file_server/stat", BaseURL, gameserverID)

	headers := map[string]string{
		"Authorization": "Bearer " + token,
	}

	params := map[string]string{}

	for i := 0; i < len(filePaths); i++ {
		params[fmt.Sprintf("files[%d]", i)] = filePaths[i]
	}

	var response GetFileDetailsResponse

	re := Request(&response, "GET", url, params, nil, headers, 0)

	if re != nil {
		return nil, re
	}

	return &response, nil
}

// GetLogFileURL func
func GetLogFileURL(token string, gameserverID int, filePath string) (*GetLogFileURLResponse, *utils.ServiceError) {
	url := fmt.Sprintf("%s/services/%d/gameservers/file_server/download", BaseURL, gameserverID)

	headers := map[string]string{
		"Authorization": "Bearer " + token,
	}

	params := map[string]string{
		"file": filePath,
	}

	var response GetLogFileURLResponse

	re := Request(&response, "GET", url, params, nil, headers, 0)

	if re != nil {
		return nil, re
	}

	return &response, nil
}

// DownloadLogFile func
func DownloadLogFile(filePath string, prevTimestamp int64, parseChatLog bool, parseAdminLog bool, parseKillLog bool) (*DownloadLogFileResponse, *utils.ServiceError) {
	logFile, logErr := RequestLogFile(filePath)

	if logErr != nil {
		return nil, logErr
	}

	return ParseLogFile(logFile, prevTimestamp, parseChatLog, parseAdminLog, parseKillLog), nil
}

// ParseLogFile func
func ParseLogFile(logFile string, prevTimestamp int64, parseChatLog bool, parseAdminLog bool, parseKillLog bool) *DownloadLogFileResponse {
	splitLog := strings.Split(strings.Replace(logFile, "\r\n", "\n", -1), "\n")

	// Get player chat lines
	// \[.*\][0-9\.\_]+\:\s[\w\d\s].*\([\w\d\s]*\)\:\s

	// Get admin command lines
	// \[.*\].{21}AdminCmd\:.

	// Get datetime substring
	// \d{4}\.\d{2}.\d{2}\-\d{2}\.\d{2}\.\d{2}

	// Parse Timestamp
	// layout := "2006.01.02_15.04.05"
	// date := "2020.07.30_00.41.12"
	// t, _ := time.Parse(layout, date)
	// t.Unix()

	var playerLogs []PlayerLog
	var adminLogs []AdminLog
	var killLogs []KillLog

	chatRe := regexp.MustCompile(`^(?P<gt>[\w\d\s\-\_]+)\s\((?P<ign>.+)\)\:\s(?P<message>.*)`)
	killLogRe := regexp.MustCompile(`^(?P<name1>[\w\d\s\-\_]+)\s\-\sLvl\s(?P<lvl1>[\d]+)\s\(?(?P<dino1>[\w\d\s\-\_]*)\)?\s?\((?P<tribe1>[\w\d\s\-\_]*)\)\s?was\skilled\sby\s?(?P<pve>a?n?)\s(?P<name2>[\w\d\s\-\_]+)\s\-\sLvl\s(?P<lvl2>[\d]+)\s\(?(?P<dino2>[\w\d\s\-\_]*)\)?\s?\((?P<tribe2>[\w\d\s\-\_]*)\)`)

	// playerRe := regexp.MustCompile(`\[.*\][0-9\.\_]+\:\s[\w\d\s].*\([\w\d\s]*\)\:\s`)
	// adminRe := regexp.MustCompile(`\[.*\].{21}AdminCmd\:.`)
	// playerNameAndChatRe := regexp.MustCompile(`\[.*\].{21}`)
	// datetimeRe := regexp.MustCompile(`\d{4}\.\d{2}.\d{2}\-\d{2}\.\d{2}\.\d{2}`)

	for _, line := range splitLog {
		if len(line) < 60 {
			continue
		}

		datetime := line[30:49]
		timestamp := getTimestamp(datetime)

		if timestamp == 0 || timestamp <= prevTimestamp {
			continue
		}

		cleanLine := line[51:]

		var chat []string

		if parseChatLog {
			chat = chatRe.FindStringSubmatch(cleanLine)
		}

		if len(chat) == 4 {
			playerLogs = append(playerLogs, PlayerLog{
				Gamertag:  chat[1],
				Name:      chat[2],
				Message:   chat[3],
				Timestamp: timestamp,
			})
		} else if parseAdminLog && cleanLine[:8] == "AdminCmd" {
			commandAndName := strings.Split(cleanLine[10:], "(PlayerName: ")

			if len(commandAndName) < 2 {
				continue
			}

			adminName := strings.Split(commandAndName[1], ",")[0]

			command := strings.TrimSpace(commandAndName[0])

			adminLogs = append(adminLogs, AdminLog{
				Name:      adminName,
				Command:   command,
				Timestamp: timestamp,
			})
		} else if parseKillLog && len(cleanLine) > 5 && cleanLine[:5] != "Tribe" && cleanLine[:5] != "Baby " && strings.Contains(cleanLine, "was killed by") {
			killLog := killLogRe.FindStringSubmatch(cleanLine)

			if killLog == nil {
				continue
			}

			/* 0 - "full-line"
			   1 - "name1"
			   2 - "lvl1"
			   3 - "dino1"
			   4 - "tribe1"
			   5 - "pve"
			   6 - "name2"
			   7 - "lvl2"
			   8 - "dino2'
			   9 - "tribe2"
			*/
			killedLevel, klErr := strconv.Atoi(killLog[2])
			if klErr != nil {
				continue
			}

			killerLevel, kerlErr := strconv.Atoi(killLog[7])
			if kerlErr != nil {
				continue
			}

			pveKill := false
			if killLog[5] != "" {
				pveKill = true
			}

			killLogs = append(killLogs, KillLog{
				PvEKill:        pveKill,
				KilledName:     killLog[1],
				KilledLevel:    killedLevel,
				KilledDinoType: killLog[3],
				KilledTribe:    killLog[4],
				KillerName:     killLog[6],
				KillerLevel:    killerLevel,
				KillerDinoType: killLog[8],
				KillerTribe:    killLog[9],
				Timestamp:      timestamp,
			})
		}
	}

	return &DownloadLogFileResponse{
		PlayerLogs: playerLogs,
		AdminLogs:  adminLogs,
		KillLogs:   killLogs,
	}
}

// getTimestamp func
func getTimestamp(datetime string) int64 {
	layout := "2006.01.02_15.04.05"

	t, err := time.Parse(layout, datetime)

	if err != nil {
		return 0
	}

	return t.Unix()
}

// GetBoostSettings func
func GetBoostSettings(token string, gameserverID int) (*BoostSettingsResponse, *utils.ServiceError) {
	url := fmt.Sprintf("%s/services/%d/gameservers/boost", BaseURL, gameserverID)

	headers := map[string]string{
		"Authorization": "Bearer " + token,
	}

	var response BoostSettingsResponse

	re := Request(&response, "GET", url, nil, nil, headers, 0)

	if re != nil {
		return nil, re
	}

	return &response, nil
}

// GetTokenInfo func
func GetTokenInfo(token string) (*TokenInfoResponse, *utils.ServiceError) {
	url := fmt.Sprintf("%s/token", BaseURL)

	headers := map[string]string{
		"Authorization": "Bearer " + token,
	}

	var response TokenInfoResponse

	re := Request(&response, "GET", url, nil, nil, headers, 0)

	if re != nil {
		return nil, re
	}

	return &response, nil
}

// PlayerWhitelist func
func PlayerWhitelist(token string, gameserverID int, playerID string) (*PlayerWhitelistResponse, *utils.ServiceError) {
	url := fmt.Sprintf("%s/services/%d/gameservers/games/whitelist", BaseURL, gameserverID)

	headers := map[string]string{
		"Authorization": "Bearer " + token,
		"Content-Type":  "application/json",
	}

	body := PlayerBanRequestBody{
		Identifier: playerID,
	}

	var response PlayerWhitelistResponse

	re := Request(&response, "POST", url, nil, body, headers, 0)

	if re != nil {
		return nil, re
	}

	return &response, nil
}

// PlayerUnwhitelist func
func PlayerUnwhitelist(token string, gameserverID int, playerID string) (*PlayerUnwhitelistResponse, *utils.ServiceError) {
	url := fmt.Sprintf("%s/services/%d/gameservers/games/whitelist", BaseURL, gameserverID)

	headers := map[string]string{
		"Authorization": "Bearer " + token,
		"Content-Type":  "application/json",
	}

	body := PlayerUnbanRequestBody{
		Identifier: playerID,
	}

	var response PlayerUnwhitelistResponse

	re := Request(&response, "DELETE", url, nil, body, headers, 0)

	if re != nil {
		return nil, re
	}

	return &response, nil
}
