package controllers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"github.com/gorilla/mux"
	"gitlab.com/BIC_Dev/nitrado-service-v2/services/nitrado"
	"gitlab.com/BIC_Dev/nitrado-service-v2/utils"
	"gitlab.com/BIC_Dev/nitrado-service-v2/viewmodels"
)

const playersTTL = "1800" // 30 minutes

// GetPlayers route
func (c *Controller) GetPlayers(w http.ResponseWriter, r *http.Request) {
	accountName := r.Header.Get("Account-Name")

	authToken, tokenErr := c.GetNitradoAuthToken(accountName)

	if tokenErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: %s: %s", tokenErr.GetMessage(), tokenErr.Error()), c.Log.LogLow)
		SendJSONResponse(w, viewmodels.NewErrorResponse(tokenErr.Error(), tokenErr.GetMessage()), tokenErr.GetStatus())
		return
	}

	vars := mux.Vars(r)
	gameserverID, convErr := strconv.Atoi(vars["gameserver_id"])

	if convErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: Could not convert gameserver_id to int: %s", convErr.Error()), c.Log.LogLow)
		SendJSONResponse(w, viewmodels.NewErrorResponse(convErr.Error(), "Could not convert gameserver_id to int"), http.StatusBadRequest)
		return
	}

	online := strings.ToLower(r.URL.Query().Get("online"))

	response, getErr := c.GetPlayersHandler(authToken, gameserverID, r.Header.Get("Cache-Control"))

	if getErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: GET PLAYERS: %d : %s (%d): %s", gameserverID, getErr.GetMessage(), getErr.GetStatus(), getErr.Error()), c.Log.LogLow)
		SendJSONResponse(w, viewmodels.NewErrorResponse(getErr.Error(), getErr.GetMessage()), getErr.StatusCode)
		return
	}

	if online == "true" {
		onlinePlayers := parseOnlinePlayers(response.Data.Players)
		response.Data.Players = onlinePlayers
	}

	playersResponse := viewmodels.GetPlayersResponse{
		Message: "Successfully retreived player list",
		Players: response.Data.Players,
	}

	SendJSONResponse(w, playersResponse, http.StatusOK)
	return
}

// SearchPlayersByName route
// Gets a list of players who have a name containing the requested name
func (c *Controller) SearchPlayersByName(w http.ResponseWriter, r *http.Request) {
	accountName := r.Header.Get("Account-Name")

	authToken, tokenErr := c.GetNitradoAuthToken(accountName)

	if tokenErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: %s: %s", tokenErr.GetMessage(), tokenErr.Error()), c.Log.LogLow)
		SendJSONResponse(w, viewmodels.NewErrorResponse(tokenErr.Error(), tokenErr.GetMessage()), tokenErr.GetStatus())
		return
	}

	vars := mux.Vars(r)
	gameserverID, convErr := strconv.Atoi(vars["gameserver_id"])

	if convErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: Could not convert gameserver_id to int: %s", convErr.Error()), c.Log.LogLow)
		SendJSONResponse(w, viewmodels.NewErrorResponse(convErr.Error(), "Could not convert gameserver_id to int"), http.StatusBadRequest)
		return
	}

	online := strings.ToLower(r.URL.Query().Get("online"))
	exactMatch := strings.ToLower(r.URL.Query().Get("exact_match"))

	var body viewmodels.SearchPlayersRequestBody
	dcErr := json.NewDecoder(r.Body).Decode(&body)

	if dcErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: Could not decode seach players body: %s", dcErr.Error()), c.Log.LogLow)
		SendJSONResponse(w, viewmodels.NewErrorResponse(dcErr.Error(), "Could not decode search players body"), http.StatusBadRequest)
		return
	}

	response, getErr := c.GetPlayersHandler(authToken, gameserverID, r.Header.Get("Cache-Control"))

	if getErr != nil {
		SendJSONResponse(w, viewmodels.NewErrorResponse(getErr.Error(), getErr.GetMessage()), getErr.StatusCode)
		return
	}

	if online == "true" {
		response.Data.Players = parseOnlinePlayers(response.Data.Players)
	}

	var foundPlayers []nitrado.Player

	if exactMatch == "true" {
		foundPlayers = searchPlayerList(response.Data.Players, body.PlayerName, true)
	} else {
		foundPlayers = searchPlayerList(response.Data.Players, body.PlayerName, false)
	}

	if len(foundPlayers) > 0 {
		SendJSONResponse(w, viewmodels.SearchPlayersResponse{
			Message: "Found players in Nitrado",
			Players: foundPlayers,
		}, http.StatusOK)
		return
	}

	SendJSONResponse(w, viewmodels.NewErrorResponse("No player found", fmt.Sprintf("Could not find player (%s) for gameserver ID: %d", body.PlayerName, gameserverID)), http.StatusNotFound)
	return
}

// BanPlayer route
func (c *Controller) BanPlayer(w http.ResponseWriter, r *http.Request) {
	accountName := r.Header.Get("Account-Name")

	authToken, tokenErr := c.GetNitradoAuthToken(accountName)

	if tokenErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: %s: %s", tokenErr.GetMessage(), tokenErr.Error()), c.Log.LogLow)
		SendJSONResponse(w, viewmodels.NewErrorResponse(tokenErr.Error(), tokenErr.GetMessage()), tokenErr.GetStatus())
		return
	}

	vars := mux.Vars(r)
	gameserverID, convErr := strconv.Atoi(vars["gameserver_id"])

	if convErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: Could not convert gameserver_id to int: %s", convErr.Error()), c.Log.LogLow)
		SendJSONResponse(w, viewmodels.NewErrorResponse(convErr.Error(), "Could not convert gameserver_id to int"), http.StatusBadRequest)
		return
	}

	var body viewmodels.BanPlayerRequestBody
	dcErr := json.NewDecoder(r.Body).Decode(&body)

	if dcErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: Could not decode ban player body: %s", dcErr.Error()), c.Log.LogLow)
		SendJSONResponse(w, viewmodels.NewErrorResponse(dcErr.Error(), "Could not decode ban player body"), http.StatusBadRequest)
		return
	}

	response, nErr := nitrado.PlayerBan(authToken, gameserverID, body.PlayerID)

	if nErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: Could not get players data for gameserver ID (%d): %s", gameserverID, nErr.Error()), c.Log.LogLow)
		SendJSONResponse(w, viewmodels.NewErrorResponse(nErr.Error(), fmt.Sprintf("Could not get players data for gameserver ID: %d", gameserverID)), http.StatusFailedDependency)
		return
	}

	SendJSONResponse(w, viewmodels.BanPlayerResponse{response}, http.StatusOK)
	return
}

// UnBanPlayer route
func (c *Controller) UnBanPlayer(w http.ResponseWriter, r *http.Request) {
	accountName := r.Header.Get("Account-Name")

	authToken, tokenErr := c.GetNitradoAuthToken(accountName)

	if tokenErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: %s: %s", tokenErr.GetMessage(), tokenErr.Error()), c.Log.LogLow)
		SendJSONResponse(w, viewmodels.NewErrorResponse(tokenErr.Error(), tokenErr.GetMessage()), tokenErr.GetStatus())
		return
	}

	vars := mux.Vars(r)
	gameserverID, convErr := strconv.Atoi(vars["gameserver_id"])

	if convErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: Could not convert gameserver_id to int: %s", convErr.Error()), c.Log.LogLow)
		SendJSONResponse(w, viewmodels.NewErrorResponse(convErr.Error(), "Could not convert gameserver_id to int"), http.StatusBadRequest)
		return
	}

	var body viewmodels.UnbanPlayerRequestBody
	dcErr := json.NewDecoder(r.Body).Decode(&body)

	if dcErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: Could not decode unban player body: %s", dcErr.Error()), c.Log.LogLow)
		SendJSONResponse(w, viewmodels.NewErrorResponse(dcErr.Error(), "Could not decode unban player body"), http.StatusBadRequest)
		return
	}

	response, nErr := nitrado.PlayerUnban(authToken, gameserverID, body.PlayerID)

	if nErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: Could not unban user for gameserver ID (%d): %s", gameserverID, nErr.Error()), c.Log.LogLow)
		SendJSONResponse(w, viewmodels.NewErrorResponse(nErr.Error(), fmt.Sprintf("Could not unban user for gameserver ID: %d", gameserverID)), http.StatusFailedDependency)
		return
	}

	SendJSONResponse(w, viewmodels.UnbanPlayerResponse{response}, http.StatusOK)
	return
}

// WhitelistPlayer route
func (c *Controller) WhitelistPlayer(w http.ResponseWriter, r *http.Request) {
	accountName := r.Header.Get("Account-Name")

	authToken, tokenErr := c.GetNitradoAuthToken(accountName)

	if tokenErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: %s: %s", tokenErr.GetMessage(), tokenErr.Error()), c.Log.LogLow)
		SendJSONResponse(w, viewmodels.NewErrorResponse(tokenErr.Error(), tokenErr.GetMessage()), tokenErr.GetStatus())
		return
	}

	vars := mux.Vars(r)
	gameserverID, convErr := strconv.Atoi(vars["gameserver_id"])

	if convErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: Could not convert gameserver_id to int: %s", convErr.Error()), c.Log.LogLow)
		SendJSONResponse(w, viewmodels.NewErrorResponse(convErr.Error(), "Could not convert gameserver_id to int"), http.StatusBadRequest)
		return
	}

	var body viewmodels.WhitelistPlayerRequestBody
	dcErr := json.NewDecoder(r.Body).Decode(&body)

	if dcErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: Could not decode whitelist player body: %s", dcErr.Error()), c.Log.LogLow)
		SendJSONResponse(w, viewmodels.NewErrorResponse(dcErr.Error(), "Could not decode ban player body"), http.StatusBadRequest)
		return
	}

	response, nErr := nitrado.PlayerWhitelist(authToken, gameserverID, body.PlayerID)

	if nErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: Could not get players data for gameserver ID (%d): %s", gameserverID, nErr.Error()), c.Log.LogLow)
		SendJSONResponse(w, viewmodels.NewErrorResponse(nErr.Error(), fmt.Sprintf("Could not get players data for gameserver ID: %d", gameserverID)), http.StatusFailedDependency)
		return
	}

	SendJSONResponse(w, viewmodels.WhitelistPlayerResponse{response}, http.StatusOK)
	return
}

// UnwhitelistPlayer route
func (c *Controller) UnwhitelistPlayer(w http.ResponseWriter, r *http.Request) {
	accountName := r.Header.Get("Account-Name")

	authToken, tokenErr := c.GetNitradoAuthToken(accountName)

	if tokenErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: %s: %s", tokenErr.GetMessage(), tokenErr.Error()), c.Log.LogLow)
		SendJSONResponse(w, viewmodels.NewErrorResponse(tokenErr.Error(), tokenErr.GetMessage()), tokenErr.GetStatus())
		return
	}

	vars := mux.Vars(r)
	gameserverID, convErr := strconv.Atoi(vars["gameserver_id"])

	if convErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: Could not convert gameserver_id to int: %s", convErr.Error()), c.Log.LogLow)
		SendJSONResponse(w, viewmodels.NewErrorResponse(convErr.Error(), "Could not convert gameserver_id to int"), http.StatusBadRequest)
		return
	}

	var body viewmodels.UnwhitelistPlayerRequestBody
	dcErr := json.NewDecoder(r.Body).Decode(&body)

	if dcErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: Could not decode whitelist player body: %s", dcErr.Error()), c.Log.LogLow)
		SendJSONResponse(w, viewmodels.NewErrorResponse(dcErr.Error(), "Could not decode ban player body"), http.StatusBadRequest)
		return
	}

	response, nErr := nitrado.PlayerUnwhitelist(authToken, gameserverID, body.PlayerID)

	if nErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: Could not get players data for gameserver ID (%d): %s", gameserverID, nErr.Error()), c.Log.LogLow)
		SendJSONResponse(w, viewmodels.NewErrorResponse(nErr.Error(), fmt.Sprintf("Could not get players data for gameserver ID: %d", gameserverID)), http.StatusFailedDependency)
		return
	}

	SendJSONResponse(w, viewmodels.UnwhitelistPlayerResponse{response}, http.StatusOK)
	return
}

// Search for players that contain a player name
func searchPlayerList(players []nitrado.Player, playerName string, exactMatch bool) []nitrado.Player {
	var foundPlayers []nitrado.Player

	for _, player := range players {
		if exactMatch {
			if strings.ToLower(player.Name) == strings.ToLower(playerName) {
				foundPlayers = append(foundPlayers, player)
			}
		} else if strings.Contains(strings.ToLower(player.Name), strings.ToLower(playerName)) {
			foundPlayers = append(foundPlayers, player)
		}
	}

	return foundPlayers
}

// Return all online players
func parseOnlinePlayers(players []nitrado.Player) []nitrado.Player {
	var onlinePlayers []nitrado.Player

	for _, player := range players {
		if player.Online {
			onlinePlayers = append(onlinePlayers, player)
		}
	}

	return onlinePlayers
}

// GetPlayersHandler func
func (c *Controller) GetPlayersHandler(authToken string, gameserverID int, cacheControl string) (*nitrado.PlayersResponse, *utils.ServiceError) {
	cacheKey := fmt.Sprintf("playerlist:%d", gameserverID)

	var cacheVal string
	var cacheErr *utils.CacheError

	if cacheControl != "no-cache" {
		// Get gameserver info from cache
		cacheVal, cacheErr = c.Cache.Get(cacheKey)
	}

	if cacheErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: %s: %s", cacheErr.GetMessage(), cacheErr.Error()), c.Log.LogMedium)
	}

	if cacheVal != "" {
		var players nitrado.PlayersResponse
		umErr := json.Unmarshal([]byte(cacheVal), &players)

		// Respond with cached gameserver info
		if umErr == nil {
			return &players, nil
		}

		c.Log.Log(fmt.Sprintf("ERROR: Unable to convert cached json players response to struct: %s", umErr.Error()), c.Log.LogMedium)
	}

	response, nErr := nitrado.GetPlayers(authToken, gameserverID)

	if nErr != nil {
		return nil, nErr
	}

	jsonData, marshalErr := json.Marshal(response)

	if marshalErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: Unable to convert PlayersResponse struct to JSON: %s", marshalErr.Error()), c.Log.LogMedium)
	} else {
		// Set TTL on cached data
		setCacheErr := c.Cache.Set(cacheKey, string(jsonData), playersTTL)

		if setCacheErr != nil {
			c.Log.Log(fmt.Sprintf("ERROR: %s: %s", setCacheErr.GetMessage(), setCacheErr.Error()), c.Log.LogMedium)
		}
	}

	return response, nil
}
