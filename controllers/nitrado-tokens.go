package controllers

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/secretsmanager"
	"github.com/google/uuid"
	"gitlab.com/BIC_Dev/nitrado-service-v2/services/nitrado"
	"gitlab.com/BIC_Dev/nitrado-service-v2/viewmodels"
)

// CreateNitradoToken route
// GET /nitrado-service-v2/nitrado-token
func (c *Controller) CreateNitradoToken(w http.ResponseWriter, r *http.Request) {
	var body viewmodels.CreateNitradoTokenRequest
	dcErr := json.NewDecoder(r.Body).Decode(&body)
	if dcErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: %s: %s", "Invalid request body for CreateNitradoToken", dcErr.Error()), c.Log.LogLow)
		SendJSONResponse(w, viewmodels.NewErrorResponse(dcErr.Error(), "Invalid request body"), http.StatusBadRequest)
		return
	}

	if body.NitradoToken == "" {
		SendJSONResponse(w, viewmodels.NewErrorResponse("invalid nitrado token", "Empty Nitrado Token in request body"), http.StatusBadRequest)
		return
	}

	newTokenInfo, ngtiErr := nitrado.GetTokenInfo(body.NitradoToken)
	if ngtiErr != nil {
		SendJSONResponse(w, viewmodels.NewErrorResponse(ngtiErr.Error(), "Invalid Nitrado Token"), http.StatusBadRequest)
		return
	}

	hasServiceScope := false
	for _, scope := range newTokenInfo.Data.Token.Scopes {
		if scope == "service" {
			hasServiceScope = true
			break
		}
	}

	if !hasServiceScope {
		SendJSONResponse(w, viewmodels.NewErrorResponse("missing service scope", "Nitrado Token is missing the service scope"), http.StatusBadRequest)
		return
	}

	for _, accountName := range body.AccountNames {
		authToken, tokenErr := c.GetNitradoAuthToken(accountName)

		if tokenErr != nil {
			if tokenErr.Error() == "not in cache" {
				continue
			}

			c.Log.Log(fmt.Sprintf("ERROR: %s: %s", tokenErr.GetMessage(), tokenErr.Error()), c.Log.LogLow)
			SendJSONResponse(w, viewmodels.NewErrorResponse(tokenErr.Error(), tokenErr.GetMessage()), tokenErr.GetStatus())
			return
		}

		tokenInfo, gtiErr := nitrado.GetTokenInfo(authToken)
		if gtiErr != nil {
			c.Log.Log(fmt.Sprintf("ERROR: Token (%s) is no longer valid: %s", accountName, gtiErr.Error()), c.Log.LogLow)
			continue
		}

		if newTokenInfo.Data.Token.User.ID == tokenInfo.Data.Token.User.ID {
			SendJSONResponse(w, viewmodels.NewErrorResponse("duplicate token", "Nitrado Token for account already exists"), http.StatusBadRequest)
			return
		}
	}

	sess := session.Must(session.NewSession(&aws.Config{
		Region: aws.String(c.Config.AWS.Region),
	}))

	svc := secretsmanager.New(sess)
	input := &secretsmanager.GetSecretValueInput{
		SecretId: aws.String(c.Config.AWS.NitradoTokensSecretArn),
	}

	result, sErr := svc.GetSecretValue(input)
	if sErr != nil {
		SendJSONResponse(w, viewmodels.NewErrorResponse(sErr.Error(), "Failed to retrieve encrypted token storage from Secrets Manager"), http.StatusFailedDependency)
		return
	}

	var tokens map[string]string

	jsonErr := json.Unmarshal([]byte(*result.SecretString), &tokens)
	if jsonErr != nil {
		SendJSONResponse(w, viewmodels.NewErrorResponse(jsonErr.Error(), "Failed to unmarshal Nitrado tokens"), http.StatusInternalServerError)
		return
	}

	newAccountName := uuid.New()

	tokens[newAccountName.String()] = body.NitradoToken

	marshaledJSON, marshalErr := json.Marshal(tokens)
	if marshalErr != nil {
		SendJSONResponse(w, viewmodels.NewErrorResponse(marshalErr.Error(), "Failed to marshal Nitrado Token secret"), http.StatusInternalServerError)
		return
	}

	marshaledString := string(marshaledJSON)

	_, ssErr := svc.UpdateSecret(&secretsmanager.UpdateSecretInput{
		SecretId:     aws.String(c.Config.AWS.NitradoTokensSecretArn),
		SecretString: &marshaledString,
	})
	if ssErr != nil {
		SendJSONResponse(w, viewmodels.NewErrorResponse(ssErr.Error(), "Failed to update Nitrado Token secret"), http.StatusFailedDependency)
		return
	}

	cacheKey := fmt.Sprintf("nitradotoken:%s", newAccountName.String())
	cErr := c.Cache.Set(cacheKey, body.NitradoToken, "")
	if cErr != nil {
		SendJSONResponse(w, viewmodels.NewErrorResponse(cErr.Error(), "Failed to add Nitrado Token to cache"), http.StatusInternalServerError)
		return
	}

	SendJSONResponse(w, viewmodels.CreateNitradoTokenResponse{
		Message:         "Successfully added Nitrado Token",
		NitradoTokenKey: newAccountName.String(),
	}, http.StatusOK)
	return
}

// UpdateNitradoToken route
// GET /nitrado-service-v2/nitrado-token
func (c *Controller) UpdateNitradoToken(w http.ResponseWriter, r *http.Request) {
	var body viewmodels.UpdateNitradoTokenRequest
	dcErr := json.NewDecoder(r.Body).Decode(&body)
	if dcErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: %s: %s", "Invalid request body for CreateNitradoToken", dcErr.Error()), c.Log.LogLow)
		SendJSONResponse(w, viewmodels.NewErrorResponse(dcErr.Error(), "Invalid request body"), http.StatusBadRequest)
		return
	}

	if body.NitradoToken == "" {
		SendJSONResponse(w, viewmodels.NewErrorResponse("invalid nitrado token", "Empty Nitrado Token in request body"), http.StatusBadRequest)
		return
	}

	newTokenInfo, ngtiErr := nitrado.GetTokenInfo(body.NitradoToken)
	if ngtiErr != nil {
		SendJSONResponse(w, viewmodels.NewErrorResponse(ngtiErr.Error(), "Invalid Nitrado Token"), http.StatusBadRequest)
		return
	}

	hasServiceScope := false
	for _, scope := range newTokenInfo.Data.Token.Scopes {
		if scope == "service" {
			hasServiceScope = true
			break
		}
	}

	if !hasServiceScope {
		SendJSONResponse(w, viewmodels.NewErrorResponse("missing service scope", "Nitrado Token is missing the service scope"), http.StatusBadRequest)
		return
	}

	services, serviceErr := nitrado.GetServices(body.NitradoToken)
	if serviceErr != nil {
		SendJSONResponse(w, viewmodels.NewErrorResponse(serviceErr.Error(), "Failed to find game servers associated with token"), http.StatusBadRequest)
		return
	}

	existingAccount := ""
	for accountName, nitradoIDs := range body.Accounts {
		authToken, tokenErr := c.GetNitradoAuthToken(accountName)

		if tokenErr != nil {
			if tokenErr.Error() == "not in cache" {
				continue
			}

			c.Log.Log(fmt.Sprintf("ERROR: %s: %s", tokenErr.GetMessage(), tokenErr.Error()), c.Log.LogLow)
			SendJSONResponse(w, viewmodels.NewErrorResponse(tokenErr.Error(), tokenErr.GetMessage()), tokenErr.GetStatus())
			return
		}

		tokenInfo, gtiErr := nitrado.GetTokenInfo(authToken)
		if gtiErr != nil {
			c.Log.Log(fmt.Sprintf("ERROR: Token (%s) is no longer valid: %s", accountName, gtiErr.Error()), c.Log.LogLow)
		} else {
			if newTokenInfo.Data.Token.User.ID == tokenInfo.Data.Token.User.ID {
				SendJSONResponse(w, viewmodels.NewErrorResponse("duplicate token", "Nitrado Token for account already exists"), http.StatusBadRequest)
				return
			}
		}

		for _, service := range services.Data.Services {
			for _, nitradoID := range nitradoIDs {
				if int64(service.ID) == nitradoID {
					existingAccount = accountName
					break
				}
			}

			if existingAccount != "" {
				break
			}
		}
	}

	sess := session.Must(session.NewSession(&aws.Config{
		Region: aws.String(c.Config.AWS.Region),
	}))

	svc := secretsmanager.New(sess)
	input := &secretsmanager.GetSecretValueInput{
		SecretId: aws.String(c.Config.AWS.NitradoTokensSecretArn),
	}

	result, sErr := svc.GetSecretValue(input)
	if sErr != nil {
		SendJSONResponse(w, viewmodels.NewErrorResponse(sErr.Error(), "Failed to retrieve encrypted token storage from Secrets Manager"), http.StatusFailedDependency)
		return
	}

	var tokens map[string]string

	jsonErr := json.Unmarshal([]byte(*result.SecretString), &tokens)
	if jsonErr != nil {
		SendJSONResponse(w, viewmodels.NewErrorResponse(jsonErr.Error(), "Failed to unmarshal Nitrado tokens"), http.StatusInternalServerError)
		return
	}

	accountName := uuid.New().String()
	if existingAccount == "" {
		tokens[accountName] = body.NitradoToken
	} else {
		accountName = existingAccount
		tokens[accountName] = body.NitradoToken
	}

	marshaledJSON, marshalErr := json.Marshal(tokens)
	if marshalErr != nil {
		SendJSONResponse(w, viewmodels.NewErrorResponse(marshalErr.Error(), "Failed to marshal Nitrado Token secret"), http.StatusInternalServerError)
		return
	}

	marshaledString := string(marshaledJSON)

	_, ssErr := svc.UpdateSecret(&secretsmanager.UpdateSecretInput{
		SecretId:     aws.String(c.Config.AWS.NitradoTokensSecretArn),
		SecretString: &marshaledString,
	})
	if ssErr != nil {
		SendJSONResponse(w, viewmodels.NewErrorResponse(ssErr.Error(), "Failed to update Nitrado Token secret"), http.StatusFailedDependency)
		return
	}

	cacheKey := fmt.Sprintf("nitradotoken:%s", accountName)
	cErr := c.Cache.Set(cacheKey, body.NitradoToken, "")
	if cErr != nil {
		SendJSONResponse(w, viewmodels.NewErrorResponse(cErr.Error(), "Failed to add Nitrado Token to cache"), http.StatusInternalServerError)
		return
	}

	output := viewmodels.UpdateNitradoTokenResponse{
		Message:         "Successfully added Nitrado Token",
		NitradoTokenKey: accountName,
	}

	if existingAccount != "" {
		output.Message = "Successfully updated Nitrado Token"
	}

	SendJSONResponse(w, output, http.StatusOK)
	return
}
