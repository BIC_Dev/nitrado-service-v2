package viewmodels

import "gitlab.com/BIC_Dev/nitrado-service-v2/services/nitrado"

// GetServicesResponse struct
type GetServicesResponse struct {
	Message  string            `json:"message"`
	Services []nitrado.Service `json:"services"`
}
