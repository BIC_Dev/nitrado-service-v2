package viewmodels

// GetFileDescriptorCountResponse struct
type GetFileDescriptorCountResponse struct {
	Current   int    `json:"current"`
	SoftLimit uint64 `json:"soft_limit"`
	MaxLimit  uint64 `json:"max_limit"`
}
