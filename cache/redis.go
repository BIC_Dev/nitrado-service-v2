package cache

import (
	"fmt"
	"net/http"

	"github.com/mediocregopher/radix/v3"
	"gitlab.com/BIC_Dev/nitrado-service-v2/utils"
)

// Cache struct
type Cache struct {
	Client *radix.Pool
	Prefix string
}

// GetClient instantiates and returns a connection pool
func GetClient(host string, port int, poolSize int) (*radix.Pool, *utils.CacheError) {
	hostWithPort := fmt.Sprintf("%s:%d", host, port)
	pool, err := radix.NewPool("tcp", hostWithPort, poolSize)

	if err != nil {
		cacheError := utils.NewCacheError(err)
		cacheError.SetMessage("Unable to create a new cache pool")
		cacheError.SetStatus(http.StatusInternalServerError)

		return nil, cacheError
	}

	return pool, nil
}

// Get a value by key
func (c *Cache) Get(key string) (string, *utils.CacheError) {
	key = fmt.Sprintf("%s:%s", c.Prefix, key)
	var getVal string
	err := c.Client.Do(radix.Cmd(&getVal, "GET", key))

	if err != nil {
		return "", &utils.CacheError{
			StatusCode: http.StatusInternalServerError,
			Err:        err,
			Message:    fmt.Sprintf("Unable to get value from Redis for key: %s", key),
		}
	}

	return getVal, nil
}

// Set a key:value pair
func (c *Cache) Set(key, value string, ttl string) *utils.CacheError {
	key = fmt.Sprintf("%s:%s", c.Prefix, key)
	err := c.Client.Do(radix.Cmd(nil, "SET", key, value))

	if err != nil {
		return &utils.CacheError{
			StatusCode: http.StatusInternalServerError,
			Err:        err,
			Message:    "Unable to set key:value pair in Redis",
		}
	}

	if ttl != "" {
		exErr := c.Client.Do(radix.Cmd(nil, "EXPIRE", key, ttl))

		if exErr != nil {
			return &utils.CacheError{
				StatusCode: http.StatusInternalServerError,
				Err:        exErr,
				Message:    "Unable to set TTL for key",
			}
		}
	}

	return nil
}
