export ENVIRONMENT=$(echo ${ENV_VARS} | jq -r '.ENVIRONMENT')
export SERVICE_TOKEN=$(echo ${ENV_VARS} | jq -r '.SERVICE_TOKEN')
export LOG_LEVEL=$(echo ${ENV_VARS} | jq -r '.LOG_LEVEL')

export NITRADO_TOKENS=$(echo ${NITRADO_TOKENS})

/main