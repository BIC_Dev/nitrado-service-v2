package viewmodels

import "gitlab.com/BIC_Dev/nitrado-service-v2/services/nitrado"

// GetBoostSettingsResponse struct
type GetBoostSettingsResponse struct {
	Message       string                `json:"message"`
	BoostSettings nitrado.BoostSettings `json:"boost_settings"`
}
