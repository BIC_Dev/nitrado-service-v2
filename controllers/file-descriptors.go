package controllers

import (
	"fmt"
	"net/http"
	"os"
	"os/exec"
	"strings"
	"syscall"

	"gitlab.com/BIC_Dev/nitrado-service-v2/viewmodels"
)

// GetFileDescriptorCounts responds with the current, soft limit, and max limit file descriptors for container
func (c *Controller) GetFileDescriptorCounts(w http.ResponseWriter, r *http.Request) {
	var limit syscall.Rlimit
	if err := syscall.Getrlimit(syscall.RLIMIT_NOFILE, &limit); err != nil {
		c.Log.Log(err.Error(), c.Log.LogLow)
		SendJSONResponse(w, viewmodels.ErrorResponse{
			StatusCode: http.StatusInternalServerError,
			Error:      err.Error(),
			Message:    "Failed to get file descriptor limits",
		}, http.StatusInternalServerError)
		return
	}

	current, cofErr := countOpenFiles()
	if cofErr != nil {
		c.Log.Log(cofErr.Error(), c.Log.LogLow)
		SendJSONResponse(w, viewmodels.ErrorResponse{
			StatusCode: http.StatusInternalServerError,
			Error:      cofErr.Error(),
			Message:    "Failed to get file descriptor count",
		}, http.StatusInternalServerError)
		return
	}

	status := viewmodels.GetFileDescriptorCountResponse{
		Current:   current,
		SoftLimit: limit.Cur,
		MaxLimit:  limit.Max,
	}

	SendJSONResponse(w, status, http.StatusOK)
}

func countOpenFiles() (int, error) {
	out, err := exec.Command("/bin/sh", "-c", fmt.Sprintf("lsof -p %v", os.Getpid())).Output()
	if err != nil {
		return 0, err
	}
	lines := strings.Split(string(out), "\n")

	return len(lines) - 1, nil
}
