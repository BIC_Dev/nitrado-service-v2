package viewmodels

// CreateNitradoTokenRequest struct
type CreateNitradoTokenRequest struct {
	NitradoToken string   `json:"nitrado_token"`
	AccountNames []string `json:"account_names"`
}

// CreateNitradoTokenResponse struct
type CreateNitradoTokenResponse struct {
	Message         string `json:"message"`
	NitradoTokenKey string `json:"nitrado_token_key"`
}

// UpdateNitradoTokenRequest struct
type UpdateNitradoTokenRequest struct {
	NitradoToken string             `json:"nitrado_token"`
	Accounts     map[string][]int64 `json:"accounts"`
}

// UpdateNitradoTokenResponse struct
type UpdateNitradoTokenResponse struct {
	Message         string `json:"message"`
	NitradoTokenKey string `json:"nitrado_token_key"`
}
