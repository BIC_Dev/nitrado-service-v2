package viewmodels

// UpdateNitradoTokensResponse struct
type UpdateNitradoTokensResponse struct {
	Message       string   `json:"message"`
	UpdatedTokens []string `json:"updated_tokens"`
	FailedTokens  []string `json:"failed_tokens"`
}
